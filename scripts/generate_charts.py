import matplotlib.pyplot as plt
import subprocess
import re
import random


def main():
    file = open("../config/primes.txt", "r")
    primes = file.read().split(",")
    numbers = []
    results = []
    print("Generate chart for " + str(len(primes)) + " primes.")
    for i in range(0, len(primes) - 1):
        for j in range(i, len(primes)):
            rand_i = i#random.randint(0, len(primes) - 1)
            rand_j = j#random.randint(0, len(primes) - 1)
            print("Number = " + str(int(primes[rand_i]) * int(primes[rand_j])) + " start!")
            config = open("temp_config.cfg", "w")
            config.write("number=" + str(int(primes[rand_i]) * int(primes[rand_j])))
            config.close()
            result = subprocess.run(['../bin/ulam', '--cpu', '-c', 'temp_config.cfg'],
                                    stdout=subprocess.PIPE).stdout.decode('utf-8')
            pattern = re.compile(r'(\d*) iterations')
            match = pattern.findall(result)
            if len(match) > 0:
                numbers.append(int(primes[rand_i]) * int(primes[rand_j]))
                results.append(int(match[0]))
            plt.plot(numbers, results, 'ro')
            plt.ylabel('iteration')
            plt.xlabel('number')
            # plt.show()
            plt.savefig("Plot.png")
            print("Number = " + str(int(primes[rand_i]) * int(primes[rand_j])) + " done!")


if __name__ == '__main__':
    main()
