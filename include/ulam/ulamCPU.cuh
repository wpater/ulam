/*
 * ulamCPU.h
 *
 *  Created on: 21 lut 2018
 *      Author: wpater
 */

#ifndef INCLUDE_ULAM_ULAMCPU_CUH_
#define INCLUDE_ULAM_ULAMCPU_CUH_

#include "model/BigNumber.cuh"
#include "model/Spiral.cuh"
#include "model/Point.cuh"
#include "ulam/utility.cuh"
#include <iostream>
#include <vector>
#include <set>
#include <chrono>
#include <ctime>

namespace ulamcpu {

vector<BigNumber> diagonals(BigNumber);

bool couldBePrime(BigNumber);

vector<BigNumber> possiblePrimes(vector<BigNumber>);

bool isDivider(BigNumber, BigNumber);

vector<BigNumber> findDividers(vector<BigNumber>, BigNumber);

bool isPrime(BigNumber);

vector<BigNumber> findPrimes(vector<BigNumber>);

set<BigNumber> resolve(BigNumber);
}

#endif /* INCLUDE_ULAM_ULAMCPU_CUH_ */
