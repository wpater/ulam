/*
 * SpiralTest.cpp
 *
 *  Created on: 14 sie 2017
 *      Author: wpater
 */

#include "gtest/gtest.cuh"
#include "model/Spiral.cuh"
#include "model/Point.cuh"
#include "model/BigNumber.cuh"

namespace {

class SpiralTest: public ::testing::Test {

protected:
    BigNumber number, x, y, elem27;
    Spiral spiral0;
    Spiral spiral16;
    Point p;

    void SetUp() {
        spiral0 = Spiral();
        spiral16 = Spiral(16);
        x = BigNumber("3");
        y = BigNumber("0");
        p = Point(x, y);
        elem27 = BigNumber("27");
        number = BigNumber();
    }

    void TearDown() {
    }
};

/**
 * Description:		Test getSize method for empty spiral.
 * Expected value: 	(int) 0
 */
TEST_F(SpiralTest, GetSize) {
    ASSERT_EQ(0, spiral0.getSize());
}

/**
 * Description:		Test getSize method for spiral with 16 elements.
 * Expected value: 	(int) 16
 */
TEST_F(SpiralTest, GetSize2) {
    ASSERT_EQ(16, spiral16.getSize());
}

/**
 * Description:		Test getNumber method for Point(3, 0).
 * Expected value: 	(BigNumber) 27
 */
TEST_F(SpiralTest, GetNumber) {
    ASSERT_TRUE(elem27 == spiral16.getNumber(p));
    ASSERT_TRUE(elem27 == spiral0.getNumber(p));
}

/**
 * Description:		Test getPoint method for BigNumber 27.
 * Expected value: 	Point(3, 0)
 */
TEST_F(SpiralTest, GetPoint) {
    ASSERT_TRUE(p == spiral16.getPoint(elem27));
    ASSERT_TRUE(p == spiral0.getPoint(elem27));
}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral1) {
    BigNumber n = BigNumber("0");
    BigNumber x_coord = BigNumber("0");
    BigNumber y_coord = BigNumber("0");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral2) {
    BigNumber n = BigNumber("1");
    BigNumber x_coord = BigNumber("1");
    BigNumber y_coord = BigNumber("0");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral3) {
    BigNumber n = BigNumber("2");
    BigNumber x_coord = BigNumber("1");
    BigNumber y_coord = BigNumber("1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral4) {
    BigNumber n = BigNumber("3");
    BigNumber x_coord = BigNumber("0");
    BigNumber y_coord = BigNumber("1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral5) {
    BigNumber n = BigNumber("4");
    BigNumber x_coord = BigNumber("-1");
    BigNumber y_coord = BigNumber("1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral6) {
    BigNumber n = BigNumber("5");
    BigNumber x_coord = BigNumber("-1");
    BigNumber y_coord = BigNumber("0");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral7) {
    BigNumber n = BigNumber("6");
    BigNumber x_coord = BigNumber("-1");
    BigNumber y_coord = BigNumber("-1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral8) {
    BigNumber n = BigNumber("7");
    BigNumber x_coord = BigNumber("0");
    BigNumber y_coord = BigNumber("-1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral9) {
    BigNumber n = BigNumber("8");
    BigNumber x_coord = BigNumber("1");
    BigNumber y_coord = BigNumber("-1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral10) {
    BigNumber n = BigNumber("9");
    BigNumber x_coord = BigNumber("2");
    BigNumber y_coord = BigNumber("-1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral11) {
    BigNumber n = BigNumber("10");
    BigNumber x_coord = BigNumber("2");
    BigNumber y_coord = BigNumber("0");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral12) {
    BigNumber n = BigNumber("11");
    BigNumber x_coord = BigNumber("2");
    BigNumber y_coord = BigNumber("1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral13) {
    BigNumber n = BigNumber("12");
    BigNumber x_coord = BigNumber("2");
    BigNumber y_coord = BigNumber("2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral14) {
    BigNumber n = BigNumber("13");
    BigNumber x_coord = BigNumber("1");
    BigNumber y_coord = BigNumber("2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral15) {
    BigNumber n = BigNumber("14");
    BigNumber x_coord = BigNumber("0");
    BigNumber y_coord = BigNumber("2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral16) {
    BigNumber n = BigNumber("15");
    BigNumber x_coord = BigNumber("-1");
    BigNumber y_coord = BigNumber("2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral17) {
    BigNumber n = BigNumber("16");
    BigNumber x_coord = BigNumber("-2");
    BigNumber y_coord = BigNumber("2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral18) {
    BigNumber n = BigNumber("17");
    BigNumber x_coord = BigNumber("-2");
    BigNumber y_coord = BigNumber("1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral19) {
    BigNumber n = BigNumber("18");
    BigNumber x_coord = BigNumber("-2");
    BigNumber y_coord = BigNumber("0");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral20) {
    BigNumber n = BigNumber("19");
    BigNumber x_coord = BigNumber("-2");
    BigNumber y_coord = BigNumber("-1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral21) {
    BigNumber n = BigNumber("20");
    BigNumber x_coord = BigNumber("-2");
    BigNumber y_coord = BigNumber("-2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral22) {
    BigNumber n = BigNumber("21");
    BigNumber x_coord = BigNumber("-1");
    BigNumber y_coord = BigNumber("-2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral23) {
    BigNumber n = BigNumber("22");
    BigNumber x_coord = BigNumber("0");
    BigNumber y_coord = BigNumber("-2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral24) {
    BigNumber n = BigNumber("23");
    BigNumber x_coord = BigNumber("1");
    BigNumber y_coord = BigNumber("-2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral25) {
    BigNumber n = BigNumber("24");
    BigNumber x_coord = BigNumber("2");
    BigNumber y_coord = BigNumber("-2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral26) {
    BigNumber n = BigNumber("25");
    BigNumber x_coord = BigNumber("3");
    BigNumber y_coord = BigNumber("-2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral27) {
    BigNumber n = BigNumber("26");
    BigNumber x_coord = BigNumber("3");
    BigNumber y_coord = BigNumber("-1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral28) {
    BigNumber n = BigNumber("27");
    BigNumber x_coord = BigNumber("3");
    BigNumber y_coord = BigNumber("0");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral29) {
    BigNumber n = BigNumber("28");
    BigNumber x_coord = BigNumber("3");
    BigNumber y_coord = BigNumber("1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral30) {
    BigNumber n = BigNumber("29");
    BigNumber x_coord = BigNumber("3");
    BigNumber y_coord = BigNumber("2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral31) {
    BigNumber n = BigNumber("30");
    BigNumber x_coord = BigNumber("3");
    BigNumber y_coord = BigNumber("3");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral32) {
    BigNumber n = BigNumber("31");
    BigNumber x_coord = BigNumber("2");
    BigNumber y_coord = BigNumber("3");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral33) {
    BigNumber n = BigNumber("32");
    BigNumber x_coord = BigNumber("1");
    BigNumber y_coord = BigNumber("3");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral34) {
    BigNumber n = BigNumber("33");
    BigNumber x_coord = BigNumber("0");
    BigNumber y_coord = BigNumber("3");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral35) {
    BigNumber n = BigNumber("34");
    BigNumber x_coord = BigNumber("-1");
    BigNumber y_coord = BigNumber("3");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral36) {
    BigNumber n = BigNumber("35");
    BigNumber x_coord = BigNumber("-2");
    BigNumber y_coord = BigNumber("3");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral37) {
    BigNumber n = BigNumber("36");
    BigNumber x_coord = BigNumber("-3");
    BigNumber y_coord = BigNumber("3");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral38) {
    BigNumber n = BigNumber("37");
    BigNumber x_coord = BigNumber("-3");
    BigNumber y_coord = BigNumber("2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral39) {
    BigNumber n = BigNumber("38");
    BigNumber x_coord = BigNumber("-3");
    BigNumber y_coord = BigNumber("1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral40) {
    BigNumber n = BigNumber("39");
    BigNumber x_coord = BigNumber("-3");
    BigNumber y_coord = BigNumber("0");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral41) {
    BigNumber n = BigNumber("40");
    BigNumber x_coord = BigNumber("-3");
    BigNumber y_coord = BigNumber("-1");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral42) {
    BigNumber n = BigNumber("41");
    BigNumber x_coord = BigNumber("-3");
    BigNumber y_coord = BigNumber("-2");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}

/**
 * Description:     Test algorithm for coordinates.
 * Expected value:  (boolean) true
 */
TEST_F(SpiralTest, Spiral43) {
    BigNumber n = BigNumber("42");
    BigNumber x_coord = BigNumber("-3");
    BigNumber y_coord = BigNumber("-3");
    Point point = Point(x_coord, y_coord);
    ASSERT_TRUE(point == spiral0.getPoint(n));
    ASSERT_TRUE(n == spiral0.getNumber(point));

}
}

