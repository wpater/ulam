Ulam - Big numbers library for GPU applied in cryptography
========================

***
Description
------------------------
This is my master thesis.

The first step in this project is to create library for big numbers (500+ bits) for GPU (CUDA).

Next step is implement Ulam spiral and alghorithm for factorization.

***
Requirements:
------------------------
* nvcc:

        $ nvcc -V
        
        nvcc: NVIDIA (R) Cuda compiler driver
        
        Copyright (c) 2005-2016 NVIDIA Corporation
        
        Built on Tue_Jan_10_13:22:03_CST_2017
        
        Cuda compilation tools, release 8.0, V8.0.61


* gcc:

        $ gcc --version
        
        gcc (Ubuntu 4.9.4-2ubuntu1~16.04) 4.9.4
        
        Copyright (C) 2015 Free Software Foundation, Inc.
    

Installation
------------------------
Clone repo and build:

    git clone https://bitbucket.org/Nequeq/ulam.git
    
    cd ulam
    
    make
    
Tests:

    ./run_tests.sh -m

Instruction: Script `run_tests.sh -h` will print instruction. ~13min all.

Run main program (only CPU for now):

    ./bin/ulam --cpu -c path/to/configuration/file

***   
Documentation
------------------------
Documentation will availbale under html directory after

    make doc

