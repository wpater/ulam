/*
 * BigNumber.cu
 *
 *  Created on: 1 paź 2017
 *      Author: wpater
 */

#include "gtest/gtest.cuh"
#include "model/BigNumber.cuh"

namespace {

class BigNumberTest: public ::testing::Test {

protected:
    BigNumber number1;
    BigNumber number2;
    std::shared_ptr<spdlog::logger> debug_logger;
    std::shared_ptr<spdlog::logger> logger;

    void SetUp() {
        debug_logger = spdlog::get("debug_logger");
        logger = spdlog::get("logger");
        debug_logger->debug("======= BigNumberTest - setup =======");
        number1 = BigNumber("1");
        number2 = BigNumber("2");
        debug_logger->debug("======= BigNumberTest - setup done =======");
    }

    void TearDown() {
        debug_logger->debug("======= BigNumberTest - cleanup =======");
        debug_logger->debug("======= BigNumberTest - cleanup done =======");
        logger->flush();
    }
};

//-------------------------------------------------------------------------------------------------------------OPERATORS
/**
 * Description:     Test - operator.
 * Expected value:  (string) "-1"
 */
TEST_F(BigNumberTest, OperatorMinus1) {
    debug_logger->debug("======= Test Operator1 - Operator - on 1. =======");
    BigNumber b = -number1;
    ASSERT_EQ("-:1", b.toString());
}

/**
 * Description:     Test - operator.
 * Expected value:  (string) "1"
 */
TEST_F(BigNumberTest, OperatorMinus2) {
    debug_logger->debug("======= Test Operator2 - Operator - on -1. =======");
    debug_logger->debug("------- Test Operator2 - set number1 to -1.");
    number1.setNumber("-1");
    BigNumber b = -number1;
    ASSERT_EQ("1", b.toString());
}

/**
 * Description:     Test == operator.
 * Expected value:  (string) "1"
 */
TEST_F(BigNumberTest, OperatorEqual1) {
    debug_logger->debug("======= Test Operator==1 - Operator ==. =======");
    debug_logger->debug("------- Test Operator==1 - set number1 to -1.");
    number1.setNumber("2");
    ASSERT_TRUE(number1 == number2);
}

/**
 * Description:     Test > operator.
 * Expected value:  (boolean) "false"
 */
TEST_F(BigNumberTest, G1) {
    debug_logger->debug("======= Test G1 - 19 > 36 =======");
    debug_logger->debug("------- Test G1 - set number1 to 19.");
    number1.setNumber("19");
    debug_logger->debug("------- Test G1 - set number2 to 36.");
    number2.setNumber("36");
    ASSERT_FALSE(number1 > number2);
}

/**
 * Description:     Test > operator.
 * Expected value:  (boolean) "true"
 */
TEST_F(BigNumberTest, G2) {
    debug_logger->debug("======= Test G2 - 36 > 19 =======");
    debug_logger->debug("------- Test G2 - set number1 to 36.");
    number1.setNumber("36");
    debug_logger->debug("------- Test G2 - set number2 to 19.");
    number2.setNumber("19");
    ASSERT_TRUE(number1 > number2);
}

/**
 * Description:     Test > operator.
 * Expected value:  (boolean) "false"
 */
TEST_F(BigNumberTest, G3) {
    debug_logger->debug("======= Test G3 - 36 > 36 =======");
    debug_logger->debug("------- Test G3 - set number1 to 36.");
    number1.setNumber("36");
    debug_logger->debug("------- Test G3 - set number2 to 36.");
    number2.setNumber("36");
    ASSERT_FALSE(number1 > number2);
}

/**
 * Description:     Test < operator.
 * Expected value:  (boolean) "true"
 */
TEST_F(BigNumberTest, L1) {
    debug_logger->debug("======= Test L1 - 19 < 36 =======");
    debug_logger->debug("------- Test L1 - set number1 to 19.");
    number1.setNumber("19");
    debug_logger->debug("------- Test L1 - set number2 to 36.");
    number2.setNumber("36");
    ASSERT_TRUE(number1 < number2);
}

/**
 * Description:     Test < operator.
 * Expected value:  (boolean) "false"
 */
TEST_F(BigNumberTest, L2) {
    debug_logger->debug("======= Test L2 - 36 < 19 =======");
    debug_logger->debug("------- Test L2 - set number1 to 36.");
    number1.setNumber("36");
    debug_logger->debug("------- Test L2 - set number2 to 19.");
    number2.setNumber("19");
    ASSERT_FALSE(number1 < number2);
}

/**
 * Description:     Test < operator.
 * Expected value:  (boolean) "false"
 */
TEST_F(BigNumberTest, L3) {
    debug_logger->debug("======= Test L3 - 36 < 36 =======");
    debug_logger->debug("------- Test L3 - set number1 to 36.");
    number1.setNumber("36");
    debug_logger->debug("------- Test L3 - set number2 to 36.");
    number2.setNumber("36");
    ASSERT_FALSE(number1 < number2);
}

/**
 * Description:     Test >= operator.
 * Expected value:  (boolean) "false"
 */
TEST_F(BigNumberTest, GoE1) {
    debug_logger->debug("======= Test GoE1 - 19 >= 36 =======");
    debug_logger->debug("------- Test GoE1 - set number1 to 19.");
    number1.setNumber("19");
    debug_logger->debug("------- Test GoE1 - set number2 to 36.");
    number2.setNumber("36");
    ASSERT_FALSE(number1 >= number2);
}

/**
 * Description:     Test >= operator.
 * Expected value:  (boolean) "true"
 */
TEST_F(BigNumberTest, GoE2) {
    debug_logger->debug("======= Test GoE2 - 36 >= 19 =======");
    debug_logger->debug("------- Test GoE2 - set number1 to 36.");
    number1.setNumber("36");
    debug_logger->debug("------- Test GoE2 - set number2 to 19.");
    number2.setNumber("19");
    ASSERT_TRUE(number1 >= number2);
}

/**
 * Description:     Test >= operator.
 * Expected value:  (boolean) "true"
 */
TEST_F(BigNumberTest, GoE3) {
    debug_logger->debug("======= Test GoE3 - 36 >= 36 =======");
    debug_logger->debug("------- Test GoE3 - set number1 to 36.");
    number1.setNumber("36");
    debug_logger->debug("------- Test GoE3 - set number2 to 36.");
    number2.setNumber("36");
    ASSERT_TRUE(number1 >= number2);
}

/**
 * Description:     Test <= operator.
 * Expected value:  (boolean) "true"
 */
TEST_F(BigNumberTest, LoE1) {
    debug_logger->debug("======= Test LoE1 - 19 <= 36 =======");
    debug_logger->debug("------- Test LoE1 - set number1 to 19.");
    number1.setNumber("19");
    debug_logger->debug("------- Test LoE1 - set number2 to 36.");
    number2.setNumber("36");
    ASSERT_TRUE(number1 <= number2);
}

/**
 * Description:     Test <= operator.
 * Expected value:  (boolean) "false"
 */
TEST_F(BigNumberTest, LoE2) {
    debug_logger->debug("======= Test LoE2 - 36 <= 19 =======");
    debug_logger->debug("------- Test LoE2 - set number1 to 36.");
    number1.setNumber("36");
    debug_logger->debug("------- Test LoE2 - set number2 to 19.");
    number2.setNumber("19");
    ASSERT_FALSE(number1 <= number2);
}

/**
 * Description:     Test <= operator.
 * Expected value:  (boolean) "true"
 */
TEST_F(BigNumberTest, LoE3) {
    debug_logger->debug("======= Test LoE3 - 36 <= 36 =======");
    debug_logger->debug("------- Test LoE3 - set number1 to 36.");
    number1.setNumber("36");
    debug_logger->debug("------- Test LoE3 - set number2 to 36.");
    number2.setNumber("36");
    ASSERT_TRUE(number1 <= number2);
}

/**
 * Description:     Test += operator.
 * Expected value:  3
 */
TEST_F(BigNumberTest, AddAndAssign1) {
    debug_logger->debug("======= Test AddAndAssign1 - 1 += 2 =======");
    number1 += number2;
    ASSERT_EQ(BigNumber("3"), number1);
}

/**
 * Description:     Test -= operator.
 * Expected value:  1
 */
TEST_F(BigNumberTest, SubtractAndAssign1) {
    debug_logger->debug("======= Test SubtractAndAssign1 - 2 -= 1 =======");
    number2 -= number1;
    ASSERT_EQ(BigNumber("1"), number2);
}

/**
 * Description:     Test *= operator.
 * Expected value:  2
 */
TEST_F(BigNumberTest, MultiplyAndAssign1) {
    debug_logger->debug("======= Test MultiplyAndAssign1 - 1 *= 2 =======");
    number1 *= number2;
    ASSERT_EQ(BigNumber("2"), number1);
}

/**
 * Description:     Test /= operator.
 * Expected value:  2
 */
TEST_F(BigNumberTest, DivideAndAssign1) {
    debug_logger->debug("======= Test DivideAndAssign1 - 2 /= 1 =======");
    number2 /= number1;
    ASSERT_EQ(BigNumber("2"), number2);
}

/**
 * Description:     Test %= operator.
 * Expected value:  1
 */
TEST_F(BigNumberTest, ModAndAssign1) {
    debug_logger->debug("======= Test ModAndAssign1 - 1 %= 2 =======");
    number1 %= number2;
    ASSERT_EQ(BigNumber("1"), number1);
}

//-------------------------------------------------------------------------------------------------------------OPERATORS

//----------------------------------------------------------------------------------------------------------------ADDING
/**
 * Description:		Test simple add for small ints.
 * Expected value: 	3
 */
TEST_F(BigNumberTest, Add1) {
    debug_logger->debug("======= Test Add1 - adding 1 to 2. =======");
    ASSERT_EQ(BigNumber("3"), number1 + number2);
}

/**
 * Description:		Test 1 + 9. Additional digit in result.
 * Expected value: 	10
 */
TEST_F(BigNumberTest, Add2) {
    debug_logger->debug("======= Test Add2 - adding 1 to 9. =======");
    debug_logger->debug("------- Test Add2 - set number2 to 9.");
    number2 = BigNumber("9");
    ASSERT_EQ(BigNumber("10"), number1 + number2);
}

/**
 * Description:		Test 9 + 9. Additional digit in result, same size.
 * Expected value: 	18
 */
TEST_F(BigNumberTest, Add3) {
    debug_logger->debug("======= Test Add3 - adding 9 to 9. =======");
    debug_logger->debug("------- Test Add3 - set number1 to 9.");
    number1 = BigNumber("9");
    debug_logger->debug("------- Test Add3 - set number2 to 9.");
    number2 = BigNumber("9");
    ASSERT_EQ(BigNumber("18"), number1 + number2);
}

/**
 * Description:		Test 19 + 9. First number has one more digit.
 * Expected value: 	28
 */
TEST_F(BigNumberTest, Add4) {
    debug_logger->debug("======= Test Add4 - adding 19 to 9. =======");
    debug_logger->debug("------- Test Add4 - set number1 to 19.");
    number1 = BigNumber("19");
    debug_logger->debug("------- Test Add4 - set number2 to 9.");
    number2 = BigNumber("9");
    ASSERT_EQ(BigNumber("28"), number1 + number2);
}

/**
 * Description:		Test 9 + 19. Second number has one more digit.
 * Expected value: 	28
 */
TEST_F(BigNumberTest, Add5) {
    debug_logger->debug("======= Test Add5 - adding 9 to 19. =======");
    debug_logger->debug("------- Test Add5 - set number1 to 9.");
    number1 = BigNumber("9");
    debug_logger->debug("------- Test Add5 - set number2 to 19.");
    number2 = BigNumber("19");
    ASSERT_EQ(BigNumber("28"), number1 + number2);
}

/**
 * Description:		Test 9999 + 2. First number has few more digits.
 * Expected value: 	10001
 */
TEST_F(BigNumberTest, Add6) {
    debug_logger->debug("======= Test Add6 - adding 9999 to 2. =======");
    debug_logger->debug("------- Test Add6 - set number1 to 9999.");
    number1 = BigNumber("9999");
    ASSERT_EQ(BigNumber("10001"), number1 + number2);
}

/**
 * Description:		Test 1 + 999999. Second number has few more digits.
 * Expected value: 	1000000
 */
TEST_F(BigNumberTest, Add7) {
    debug_logger->debug("======= Test Add7 - adding 1 to 999999. =======");
    debug_logger->debug("------- Test Add7 - set number2 to 999999.");
    number2 = BigNumber("999999");
    ASSERT_EQ(BigNumber("1000000"), number1 + number2);
}

/**
 * Description:     Test 1 + (-1). Add minus.
 * Expected value:  0
 */
TEST_F(BigNumberTest, Add8) {
    debug_logger->debug("======= Test Add8 - adding 1 and -1. =======");
    debug_logger->debug("------- Test Add8 - set number2 to -1.");
    number2 = BigNumber("-1");
    ASSERT_EQ(BigNumber("0"), number1 + number2);
}

/**
 * Description:     Test 3 + (-7). Add minus.
 * Expected value:  -4
 */
TEST_F(BigNumberTest, Add9) {
    debug_logger->debug("======= Test Add9 - adding 3 and -7. =======");
    debug_logger->debug("------- Test Add9 - set number1 to 3.");
    number1 = BigNumber("3");
    debug_logger->debug("------- Test Add9 - set number2 to -7.");
    number2 = BigNumber("-7");
    ASSERT_EQ(BigNumber("-4"), number1 + number2);
}

/**
 * Description:     Test 7 + (-3). Add minus.
 * Expected value:  4
 */
TEST_F(BigNumberTest, Add10) {
    debug_logger->debug("======= Test Add10 - adding 7 and -3. =======");
    debug_logger->debug("------- Test Add10 - set number1 to 7.");
    number1 = BigNumber("7");
    debug_logger->debug("------- Test Add10 - set number2 to -3.");
    number2 = BigNumber("-3");
    ASSERT_EQ(BigNumber("4"), number1 + number2);
}

/**
 * Description:     Test -7 + 3. Minus add plus.
 * Expected value:  -4
 */
TEST_F(BigNumberTest, Add11) {
    debug_logger->debug("======= Test Add11 - adding -7 and 3. =======");
    debug_logger->debug("------- Test Add11 - set number1 to -7.");
    number1 = BigNumber("-7");
    debug_logger->debug("------- Test Add11 - set number2 to 3.");
    number2 = BigNumber("3");
    ASSERT_EQ(BigNumber("-4"), number1 + number2);
}

/**
 * Description:     Test -3 + 7. Minus add plus.
 * Expected value:  4
 */
TEST_F(BigNumberTest, Add12) {
    debug_logger->debug("======= Test Add12 - adding -3 and 7. =======");
    debug_logger->debug("------- Test Add12 - set number1 to -3.");
    number1 = BigNumber("-3");
    debug_logger->debug("------- Test Add12 - set number2 to 7.");
    number2 = BigNumber("7");
    ASSERT_EQ(BigNumber("4"), number1 + number2);
}

/**
 * Description:     Test -12 + 7. Minus add plus.
 * Expected value:  -5
 */
TEST_F(BigNumberTest, Add13) {
    debug_logger->debug("======= Test Add13 - adding -12 and 7. =======");
    debug_logger->debug("------- Test Add13 - set number1 to -12.");
    number1 = BigNumber("-12");
    debug_logger->debug("------- Test Add13 - set number2 to 7.");
    number2 = BigNumber("7");
    ASSERT_EQ(BigNumber("-5"), number1 + number2);
}

/**
 * Description:     Test 17 + (-9) . Add minus.
 * Expected value:  8
 */
TEST_F(BigNumberTest, Add14) {
    debug_logger->debug("======= Test Add14 - adding 17 and -9. =======");
    debug_logger->debug("------- Test Add14 - set number1 to 17.");
    number1 = BigNumber("17");
    debug_logger->debug("------- Test Add14 - set number2 to -9.");
    number2 = BigNumber("-9");
    ASSERT_EQ(BigNumber("8"), number1 + number2);
}

/**
 * Description:     Test 111 + 100 . Add in base2.
 * Expected value:  1011
 */
TEST_F(BigNumberTest, Add15) {
    debug_logger->debug("======= Test Add15 - adding 111 and 100 in base2. =======");
    debug_logger->debug("------- Test Add15 - set number1 to 111.");
    number1 = BigNumber(2, "111");
    debug_logger->debug("------- Test Add15 - set number2 to 100.");
    number2 = BigNumber(2, "100");
    ASSERT_EQ(BigNumber(2, "1011"), number1 + number2);
}

/**
 * Description:     Test 1:7:16 (424) + 1:3 (20). Add in base17.
 * Expected value:  1:9:2(444)
 */
TEST_F(BigNumberTest, Add16) {
    debug_logger->debug("======= Test Add16 - adding 1:7:16 and 1:3 in base17. =======");
    debug_logger->debug("------- Test Add16 - set number1 to 1:7:16.");
    number1 = BigNumber(17, "1:7:16");
    debug_logger->debug("------- Test Add16 - set number2 to 1:3.");
    number2 = BigNumber(17, "1:3");
    ASSERT_EQ(BigNumber("444"), number1 + number2);
}

/**
 * Description:     Test 14 + (-10). Add in base10.
 * Expected value:  4
 */
TEST_F(BigNumberTest, Add17) {
    debug_logger->debug("======= Test Add17 - adding 14 and -10 in base10. =======");
    debug_logger->debug("------- Test Add17 - set number1 to 14.");
    number1 = BigNumber(10, "14", false);
    debug_logger->debug("------- Test Add17 - set number2 to -10.");
    number2 = BigNumber(10, "-:10", false);
    ASSERT_EQ(BigNumber(10, "4", false), number1 + number2);
}

//----------------------------------------------------------------------------------------------------------------ADDING

//-----------------------------------------------------------------------------------------------------------SUBTRACTING
/**
 * Description:		Test 1 - 1. Subtract same numbers.
 * Expected value: 	0
 */
TEST_F(BigNumberTest, Sub1) {
    debug_logger->debug("======= Test Sub1 - subtracting 1 and 1. =======");
    ASSERT_EQ(BigNumber("0"), number1 - number1);
}

/**
 * Description:		Test 2 - 1. Subtract simple ints.
 * Expected value: 	1
 */
TEST_F(BigNumberTest, Sub2) {
    debug_logger->debug("======= Test Sub2 - subtracting 2 and 1. =======");
    ASSERT_EQ(BigNumber("1"), number2 - number1);
}

/**
 * Description:		Test 10 - 2. Less digits in result.
 * Expected value: 	8
 */
TEST_F(BigNumberTest, Sub3) {
    debug_logger->debug("======= Test Sub3 - subtracting 10 and 2. =======");
    debug_logger->debug("------- Test Sub3 - set number1 to 10.");
    number1 = BigNumber("10");
    ASSERT_EQ(BigNumber("8"), number1 - number2);
}

/**
 * Description:		Test 100 - 2. Less digits in result.
 * Expected value: 	98
 */
TEST_F(BigNumberTest, Sub4) {
    debug_logger->debug("======= Test Sub4 - subtracting 100 and 2. =======");
    debug_logger->debug("------- Test Sub4 - set number1 to 100.");
    number1 = BigNumber("100");
    ASSERT_EQ(BigNumber("98"), number1 - number2);
}

/**
 * Description:		Test 100 - 98. Less digits in result.
 * Expected value: 	2
 */
TEST_F(BigNumberTest, Sub5) {
    debug_logger->debug("======= Test Sub5 - subtracting 100 and 98. =======");
    debug_logger->debug("------- Test Sub5 - set number1 to 100.");
    number1 = BigNumber("100");
    debug_logger->debug("------- Test Sub5 - set number2 to 98.");
    number2 = BigNumber("98");
    ASSERT_EQ(BigNumber("2"), number1 - number2);
}

/**
 * Description:		Test 1 - 2. Minus.
 * Expected value: 	-1
 */
TEST_F(BigNumberTest, Sub6) {
    debug_logger->debug("======= Test Sub6 - subtracting 1 and 2. =======");
    ASSERT_EQ(BigNumber("-1"), number1 - number2);
}

/**
 * Description:		Test 22 - 111. Minus, more digits.
 * Expected value: 	-89
 */
TEST_F(BigNumberTest, Sub7) {
    debug_logger->debug("======= Test Sub7 - subtracting 22 and 111. =======");
    debug_logger->debug("------- Test Sub7 - set number1 to 22.");
    number1 = BigNumber("22");
    debug_logger->debug("------- Test Sub7 - set number2 to 111.");
    number2 = BigNumber("111");
    ASSERT_EQ(BigNumber("-89"), number1 - number2);
}

/**
 * Description:		Test 123456789 - 7654. Few digits.
 * Expected value: 	123449135
 */
TEST_F(BigNumberTest, Sub8) {
    debug_logger->debug("======= Test Sub8 - subtracting 123456789 and 7654. =======");
    debug_logger->debug("------- Test Sub8 - set number1 to 123456789.");
    number1 = BigNumber("123456789");
    debug_logger->debug("------- Test Sub8 - set number2 to 7654.");
    number2 = BigNumber("7654");
    ASSERT_EQ(BigNumber("123449135"), number1 - number2);
}

/**
 * Description:		Test 7654 - 123456789. Few digits and minus.
 * Expected value: 	-123449135
 */
TEST_F(BigNumberTest, Sub9) {
    debug_logger->debug("======= Test Sub9 - subtracting 7654 and 123456789. =======");
    debug_logger->debug("------- Test Sub9 - set number1 to 7654.");
    number1 = BigNumber("7654");
    debug_logger->debug("------- Test Sub9 - set number2 to 123456789.");
    number2 = BigNumber("123456789");
    ASSERT_EQ(BigNumber("-123449135"), number1 - number2);
}

/**
 * Description:     Test 7 - (-3) . Minus minus.
 * Expected value:  10
 */
TEST_F(BigNumberTest, Sub10) {
    debug_logger->debug("======= Test Sub10 - subtracting 7 and -3. =======");
    debug_logger->debug("------- Test Sub10 - set number1 to 7.");
    number1 = BigNumber("7");
    debug_logger->debug("------- Test Sub10 - set number2 to -3.");
    number2 = BigNumber("-3");
    ASSERT_EQ(BigNumber("10"), number1 - number2);
}

/**
 * Description:     Test -7 - 3 . Minus minus.
 * Expected value:  -10
 */
TEST_F(BigNumberTest, Sub11) {
    debug_logger->debug("======= Test Sub11 - subtracting -7 and 3. =======");
    debug_logger->debug("------- Test Sub11 - set number1 to -7.");
    number1 = BigNumber("-7");
    debug_logger->debug("------- Test Sub11 - set number2 to 3.");
    number2 = BigNumber("3");
    ASSERT_EQ(BigNumber("-10"), number1 - number2);
}

/**
 * Description:     Test -7 - (-3) . Minus minus.
 * Expected value:  -4
 */
TEST_F(BigNumberTest, Sub12) {
    debug_logger->debug("======= Test Sub12 - subtracting -7 and -3. =======");
    debug_logger->debug("------- Test Sub12 - set number1 to -7.");
    number1 = BigNumber("-7");
    debug_logger->debug("------- Test Sub12 - set number2 to -3.");
    number2 = BigNumber("-3");
    ASSERT_EQ(BigNumber("-4"), number1 - number2);
}

/**
 * Description:     Test 3 - (-7) . Minus minus.
 * Expected value:  10
 */
TEST_F(BigNumberTest, Sub13) {
    debug_logger->debug("======= Test Sub13 - subtracting 3 and -7. =======");
    debug_logger->debug("------- Test Sub13 - set number1 to 3.");
    number1 = BigNumber("3");
    debug_logger->debug("------- Test Sub13 - set number2 to -7.");
    number2 = BigNumber("-7");
    ASSERT_EQ(BigNumber("10"), number1 - number2);
}

/**
 * Description:     Test -3 - 7 . Minus minus.
 * Expected value:  -10
 */
TEST_F(BigNumberTest, Sub14) {
    debug_logger->debug("======= Test Sub14 - subtracting -3 and 7. =======");
    debug_logger->debug("------- Test Sub14 - set number1 to -3.");
    number1 = BigNumber("-3");
    debug_logger->debug("------- Test Sub14 - set number2 to 7.");
    number2 = BigNumber("7");
    ASSERT_EQ(BigNumber("-10"), number1 - number2);
}

/**
 * Description:     Test -3 - (-7) . Minus minus.
 * Expected value:  4
 */
TEST_F(BigNumberTest, Sub15) {
    debug_logger->debug("======= Test Sub15 - subtracting -3 and -7. =======");
    debug_logger->debug("------- Test Sub15 - set number1 to -3.");
    number1 = BigNumber("-3");
    debug_logger->debug("------- Test Sub15 - set number2 to -7.");
    number2 = BigNumber("-7");
    ASSERT_EQ(BigNumber("4"), number1 - number2);
}

/**
 * Description:     Test -171 - (-39) . Minus minus.
 * Expected value:  -132
 */
TEST_F(BigNumberTest, Sub16) {
    debug_logger->debug("======= Test Sub16 - subtracting -171 and -39. =======");
    debug_logger->debug("------- Test Sub16 - set number1 to -171.");
    number1 = BigNumber("-171");
    debug_logger->debug("------- Test Sub16 - set number2 to -39.");
    number2 = BigNumber("-39");
    ASSERT_EQ(BigNumber("-132"), number1 - number2);
}

/**
 * Description:     Test 25 - 26 . Case for -1.
 * Expected value:  -1
 */
TEST_F(BigNumberTest, Sub17) {
    debug_logger->debug("======= Test Sub17 - subtracting 25 and 26. =======");
    debug_logger->debug("------- Test Sub17 - set number1 to 25.");
    number1 = BigNumber("25");
    debug_logger->debug("------- Test Sub17 - set number2 to 26.");
    number2 = BigNumber("26");
    ASSERT_EQ(BigNumber("-1"), number1 - number2);
}

/**
 * Description:     Test 1107 - 41.
 * Expected value:  1066
 */
TEST_F(BigNumberTest, Sub18) {
    debug_logger->debug("======= Test Sub18 - subtracting 1107 and 41. =======");
    debug_logger->debug("------- Test Sub18 - set number1 to 1107.");
    number1 = BigNumber("1107");
    debug_logger->debug("------- Test Sub18 - set number2 to 41.");
    number2 = BigNumber("41");
    ASSERT_EQ(BigNumber("1066"), number1 - number2);
}

/**
 * Description:     Test 111 (7) - 100 (4) . Sub in base2.
 * Expected value:  11 (3)
 */
TEST_F(BigNumberTest, Sub19) {
    debug_logger->debug("======= Test Sub19 - adding 111 and 100 in base2. =======");
    debug_logger->debug("------- Test Sub19 - set number1 to 111.");
    number1 = BigNumber(2, "111");
    debug_logger->debug("------- Test Sub19 - set number2 to 100.");
    number2 = BigNumber(2, "100");
    ASSERT_EQ(BigNumber(2, "11"), number1 - number2);
}

/**
 * Description:     Test 1:7:16 (424) - 1:3 (20). Sub in base17.
 * Expected value:  1:6:13(404)
 */
TEST_F(BigNumberTest, Sub20) {
    debug_logger->debug("======= Test Sub20 - adding 1:7:16 and 1:3 in base17. =======");
    debug_logger->debug("------- Test Sub20 - set number1 to 1:7:16.");
    number1 = BigNumber(17, "1:7:16");
    debug_logger->debug("------- Test Sub20 - set number2 to 1:3.");
    number2 = BigNumber(17, "1:3");
    ASSERT_EQ(BigNumber(17, "1:6:13"), number1 - number2);
}

//-----------------------------------------------------------------------------------------------------------SUBTRACTING

//-----------------------------------------------------------------------------------------------------------MULTIPLYING
/**
 * Description:		Test 3 * 2. Simple multiply.
 * Expected value: 	6
 */
TEST_F(BigNumberTest, Mul1) {
    debug_logger->debug("======= Test Mul1 - multiplying 3 and 2. =======");
    debug_logger->debug("------- Test Mul1 - set number1 to 2.");
    number1 = BigNumber("3");
    ASSERT_EQ(BigNumber("6"), number1 * number2);
}

/**
 * Description:		Test 222 * 222. Simple multiply.
 * Expected value: 	49284
 */
TEST_F(BigNumberTest, Mul2) {
    debug_logger->debug("======= Test Mul2 - multiplying 222 and 222. =======");
    debug_logger->debug("------- Test Mul2 - set number1 to 222.");
    number1 = BigNumber("222");
    debug_logger->debug("------- Test Mul2 - set number2 to 222.");
    number2 = BigNumber("222");
    ASSERT_EQ(BigNumber("49284"), number1 * number2);
}

/**
 * Description:		Test 1 * 3. Simple multiply.
 * Expected value: 	3
 */
TEST_F(BigNumberTest, Mul3) {
    debug_logger->debug("======= Test Mul3 - multiplying 1 and 3. =======");
    debug_logger->debug("------- Test Mul3 - set number2 to 3.");
    number2 = BigNumber("3");
    ASSERT_EQ(BigNumber("3"), number1 * number2);
}

/**
 * Description:     Test -1 * 2. Simple multiply with minuses.
 * Expected value:  -2
 */
TEST_F(BigNumberTest, Mul4) {
    debug_logger->debug("======= Test Mul4 - multiplying -1 and 2. =======");
    debug_logger->debug("------- Test Mul4 - set number1 to -1.");
    number1 = BigNumber("-1");
    ASSERT_EQ(BigNumber("-2"), number1 * number2);
}

/**
 * Description:     Test 1 * -2. Simple multiply with minuses.
 * Expected value:  -2
 */
TEST_F(BigNumberTest, Mul5) {
    debug_logger->debug("======= Test Mul5 - multiplying -1 and 2. =======");
    debug_logger->debug("------- Test Mul5 - set number2 to -2.");
    number2 = BigNumber("-2");
    ASSERT_EQ(BigNumber("-2"), number1 * number2);
}

/**
 * Description:     Test -1 * -2. Simple multiply with minuses.
 * Expected value:  2
 */
TEST_F(BigNumberTest, Mul6) {
    debug_logger->debug("======= Test Mul6 - multiplying -1 and -2. =======");
    debug_logger->debug("------- Test Mul6 - set number1 to -1.");
    number1 = BigNumber("-1");
    debug_logger->debug("------- Test Mul6 - set number2 to -2.");
    number2 = BigNumber("-2");
    ASSERT_EQ(BigNumber("2"), number1 * number2);
}

/**
 * Description:     Test 0 * 2. Simple multiply with zero.
 * Expected value:  0
 */
TEST_F(BigNumberTest, Mul7) {
    debug_logger->debug("======= Test Mul7 - multiplying 0 and 2. =======");
    debug_logger->debug("------- Test Mul7 - set number1 to 0.");
    number1 = BigNumber("0");
    ASSERT_EQ(BigNumber("0"), number1 * number2);
}

/**
 * Description:     Test 1 * 0. Simple multiply with zero.
 * Expected value:  0
 */
TEST_F(BigNumberTest, Mul8) {
    debug_logger->debug("======= Test Mul8 - multiplying 1 and 0. =======");
    debug_logger->debug("------- Test Mul8 - set number2 to 0.");
    number2 = BigNumber("0");
    ASSERT_EQ(BigNumber("0"), number1 * number2);
}

/**
 * Description:     Test 0 * 0. Simple multiply with zero.
 * Expected value:  0
 */
TEST_F(BigNumberTest, Mul9) {
    debug_logger->debug("======= Test Mul9 - multiplying 0 and 0. =======");
    debug_logger->debug("------- Test Mul9 - set number1 to 0.");
    number1 = BigNumber("0");
    debug_logger->debug("------- Test Mul9 - set number2 to 0.");
    number2 = BigNumber("0");
    ASSERT_EQ(BigNumber("0"), number1 * number2);
}

/**
 * Description:     Test 151 * 139. Simple multiply with zero.
 * Expected value:  20989
 */
TEST_F(BigNumberTest, Mul10) {
    debug_logger->debug("======= Test Mul10 - multiplying 151 and 139. =======");
    debug_logger->debug("------- Test Mul10 - set number1 to 151.");
    number1 = BigNumber("151");
    debug_logger->debug("------- Test Mul10 - set number2 to 139.");
    number2 = BigNumber("139");
    ASSERT_EQ(BigNumber("20989"), number1 * number2);
}

///**
// * Description:     Test RSA-100 (330 bits).
// * Expected value:  (string) "1522605027922533360535618378132637429718068114961380688657908494580122963258952897654000350692006139"
// */
//TEST_F(BigNumberTest, Mul11) {
//    debug_logger->debug("======= Test Mul10 - RSA 100. =======");
//    number1 = BigNumber("37975227936943673922808872755445627854565536638199");
//    number2 = BigNumber("40094690950920881030683735292761468389214899724061");
//    ASSERT_EQ("1522605027922533360535618378132637429718068114961380688657908494580122963258952897654000350692006139",
//            number1.multiply(number2).toString());
//}

///**
// * Description:     Test RSA-200 (663 bits).
// * Expected value:  (string)
// * "2799783391122132787082946763872260162107044678695542853756000992932612840010
// * 7609345671052955360856061822351910951365788637105954482006576775098580557613
// * 579098734950144178863178946295187237869221823983"
// */
//TEST_F(BigNumberTest, Mul11) {
//    debug_logger->debug("======= Test Mul11 - RSA 200. =======");
//    number1 = BigNumber("3532461934402770121272604978198464368671197400197625023649303468776121253679423200058547956528088349");
//    number2 = BigNumber("7925869954478333033347085841480059687737975857364219960734330341455767872818152135381409304740185467");
//    ASSERT_EQ("27997833911221327870829467638722601621070446786955428537560009929326128400107609345671052955360856061822351910951365788637105954482006576775098580557613579098734950144178863178946295187237869221823983"),
//            number1.multiply(number2).toString()));
//}

//-----------------------------------------------------------------------------------------------------------MULTIPLYING

//--------------------------------------------------------------------------------------------------------------DIVIDING
/**
 * Description:		Test 3 / 2. Simple div.
 * Expected value: 	1
 */
TEST_F(BigNumberTest, Div1) {
    debug_logger->debug("======= Test Div1 - Div 3 and 2. =======");
    debug_logger->debug("------- Test Div1 - set number1 to 2.");
    number1 = BigNumber("3");
    ASSERT_EQ(BigNumber("1"), number1 / number2);
}

/**
 * Description:		Test 1 / 2. Simple div.
 * Expected value: 	0
 */
TEST_F(BigNumberTest, Div2) {
    debug_logger->debug("======= Test Div2 - Div 1 and 2. =======");
    ASSERT_EQ(BigNumber("0"), number1 / number2);
}

/**
 * Description:		Test 300 / 50. Simple div.
 * Expected value: 	6
 */
TEST_F(BigNumberTest, Div3) {
    debug_logger->debug("======= Test Div3 - Div 3 and 2. =======");
    debug_logger->debug("------- Test Div3 - set number1 to 300.");
    number1 = BigNumber("300");
    debug_logger->debug("------- Test Div3 - set number2 to 50.");
    number2 = BigNumber("50");
    ASSERT_EQ(BigNumber("6"), number1 / number2);
}

/**
 * Description:     Test -300 / 50. Simple div with minuses.
 * Expected value:  -6
 */
TEST_F(BigNumberTest, Div4) {
    debug_logger->debug("======= Test Div4 - Div -300 and 50. =======");
    debug_logger->debug("------- Test Div4 - set number1 to -300.");
    number1 = BigNumber("-300");
    debug_logger->debug("------- Test Div4 - set number2 to 50.");
    number2 = BigNumber("50");
    ASSERT_EQ(BigNumber("-6"), number1 / number2);
}

/**
 * Description:     Test 300 / -50. Simple div with minuses.
 * Expected value:  -6
 */
TEST_F(BigNumberTest, Div5) {
    debug_logger->debug("======= Test Div5 - Div 300 and -50. =======");
    debug_logger->debug("------- Test Div5 - set number1 to 300.");
    number1 = BigNumber("300");
    debug_logger->debug("------- Test Div5 - set number2 to -50.");
    number2 = BigNumber("-50");
    ASSERT_EQ(BigNumber("-6"), number1 / number2);
}

/**
 * Description:     Test -300 / -50. Simple div with minuses.
 * Expected value:  6
 */
TEST_F(BigNumberTest, Div6) {
    debug_logger->debug("======= Test Div6 - Div -300 and 50. =======");
    debug_logger->debug("------- Test Div6 - set number1 to -300.");
    number1 = BigNumber("-300");
    debug_logger->debug("------- Test Div6 - set number2 to -50.");
    number2 = BigNumber("-50");
    ASSERT_EQ(BigNumber("6"), number1 / number2);
}

/**
 * Description:     Test 0 / 0.
 * Expected value:  Exception
 */
TEST_F(BigNumberTest, Div7) {
    debug_logger->debug("======= Test Div7 - Div 0 and 0. =======");
    debug_logger->debug("------- Test Div7 - set number1 to 0.");
    number1 = BigNumber("0");
    debug_logger->debug("------- Test Div7 - set number2 to 0.");
    number2 = BigNumber("0");
    try {
        number1 / number2;
        FAIL();
    } catch (const std::invalid_argument& e) {
        SUCCEED();
    }
}

/**
 * Description:     Test 0 / 2.
 * Expected value:  0
 */
TEST_F(BigNumberTest, Div8) {
    debug_logger->debug("======= Test Div8 - Div 0 and 2. =======");
    debug_logger->debug("------- Test Div8 - set number1 to 0.");
    number1 = BigNumber("0");
    ASSERT_EQ(BigNumber("0"), number1 / number2);
}

/**
 * Description:     Test 1 / 0.
 * Expected value:  Exception
 */
TEST_F(BigNumberTest, Div9) {
    debug_logger->debug("======= Test Div9 - Div 1 and 0. =======");
    debug_logger->debug("------- Test Div9 - set number2 to 0.");
    number2 = BigNumber("0");
    try {
        number1 / number2;
        FAIL();
    } catch (const std::invalid_argument& e) {
        SUCCEED();
    }
}

/**
 * Description:     Test 1763 / 41.
 * Expected value:  43
 */
TEST_F(BigNumberTest, Div10) {
    debug_logger->debug("======= Test Div10 - Div 1763 and 41. =======");
    debug_logger->debug("------- Test Div10 - set number1 to 1763.");
    number1 = BigNumber("1763");
    debug_logger->debug("------- Test Div10 - set number2 to 41.");
    number2 = BigNumber("41");
    ASSERT_EQ(BigNumber("43"), number1 / number2);
}

//--------------------------------------------------------------------------------------------------------------DIVIDING

//--------------------------------------------------------------------------------------------------------------SQUARING
/**
 * Description:     Test square.
 * Expected value:  (string) "2"
 */
TEST_F(BigNumberTest, Square1) {
    debug_logger->debug("======= Test Square1 - sqrt on 4 =======");
    debug_logger->debug("------- Test Square1 - set number1 to 4.");
    number1 = BigNumber("4");
    ASSERT_EQ(BigNumber("2"), number1.sqrt());
}

/**
 * Description:     Test square.
 * Expected value:  (string) "6"
 */
TEST_F(BigNumberTest, Square2) {
    debug_logger->debug("======= Test Square2 - sqrt on 36 =======");
    debug_logger->debug("------- Test Square2 - set number1 to 36.");
    number1 = BigNumber("36");
    ASSERT_EQ(BigNumber("6"), number1.sqrt());
}

/**
 * Description:     Test square.
 * Expected value:  (string) "5"
 */
TEST_F(BigNumberTest, Square3) {
    debug_logger->debug("======= Test Square3 - sqrt on 26 =======");
    debug_logger->debug("------- Test Square3 - set number1 to 26.");
    number1 = BigNumber("26");
    ASSERT_EQ(BigNumber("5"), number1.sqrt());
}

/**
 * Description:     Test square.
 * Expected value:  (string) "4"
 */
TEST_F(BigNumberTest, Square4) {
    debug_logger->debug("======= Test Square4 - sqrt on 24 =======");
    debug_logger->debug("------- Test Square4 - set number1 to 24.");
    number1 = BigNumber("24");
    ASSERT_EQ(BigNumber("4"), number1.sqrt());
}

/**
 * Description:     Test square.
 * Expected value:  (string) "5"
 */
TEST_F(BigNumberTest, Square5) {
    debug_logger->debug("======= Test Square5 - sqrt on 30 =======");
    debug_logger->debug("------- Test Square5 - set number1 to 30.");
    number1 = BigNumber("30");
    ASSERT_EQ(BigNumber("5"), number1.sqrt());
}

/**
 * Description:     Test square.
 * Expected value:  (string) "5"
 */
TEST_F(BigNumberTest, Square6) {
    debug_logger->debug("======= Test Square6 - sqrt on 31 =======");
    debug_logger->debug("------- Test Square6 - set number1 to 31.");
    number1 = BigNumber("31");
    //ASSERT_EQ("6"), number1.sqrt().toString()));
    ASSERT_EQ(BigNumber("5"), number1.sqrt());
}

/**
 * Description:     Test square.
 * Expected value:  (string) "2"
 */
TEST_F(BigNumberTest, Square7) {
    debug_logger->debug("======= Test Square7 - sqrt on 6 =======");
    debug_logger->debug("------- Test Square7 - set number1 to 6.");
    number1 = BigNumber("6");
    ASSERT_EQ(BigNumber("2"), number1.sqrt());
}

//--------------------------------------------------------------------------------------------------------------SQUARING

//-------------------------------------------------------------------------------------------------------------------MOD

/**
 * Description:     Test mod.
 * Expected value:  2
 */
TEST_F(BigNumberTest, Mod1) {
    debug_logger->debug("======= Test Mod1 - 5 mod 3 =======");
    debug_logger->debug("------- Test Mod1 - set number1 to 5.");
    number1 = BigNumber("5");
    debug_logger->debug("------- Test Mod1 - set number2 to 3.");
    number2 = BigNumber("3");
    ASSERT_EQ(BigNumber("2"), number1 % number2);
}

/**
 * Description:     Test mod.
 * Expected value:  1
 */
TEST_F(BigNumberTest, Mod2) {
    debug_logger->debug("======= Test Mod2 - 10 mod 3 =======");
    debug_logger->debug("------- Test Mod2 - set number1 to 10.");
    number1 = BigNumber("10");
    debug_logger->debug("------- Test Mod2 - set number2 to 3.");
    number2 = BigNumber("3");
    ASSERT_EQ(BigNumber("1"), number1 % number2);
}

/**
 * Description:     Test mod.
 * Expected value:  1
 */
TEST_F(BigNumberTest, Mod3) {
    debug_logger->debug("======= Test Mod2 - 1 mod 2 =======");
    ASSERT_EQ(BigNumber("1"), number1 % number2);
}

/**
 * Description:     Test mod.
 * Expected value:  0
 */
TEST_F(BigNumberTest, Mod4) {
    debug_logger->debug("======= Test Mod4 - 2 mod 2 =======");
    debug_logger->debug("------- Test Mod4 - set number1 to 2.");
    number1 = BigNumber("2");
    ASSERT_EQ(BigNumber("0"), number1 % number2);
}

//-------------------------------------------------------------------------------------------------------------------MOD

//---------------------------------------------------------------------------------------------------------------IS EVEN

/**
 * Description:     Test is even.
 * Expected value:  true
 */
TEST_F(BigNumberTest, Even1) {
    debug_logger->debug("======= Test Even1 - 2 =======");
    ASSERT_TRUE(number2.isEven());
}

/**
 * Description:     Test is even.
 * Expected value:  true
 */
TEST_F(BigNumberTest, Even2) {
    debug_logger->debug("======= Test Even2 - 12 =======");
    debug_logger->debug("------- Test Even2- set number1 to 12.");
    number1 = BigNumber("12");
    ASSERT_TRUE(number1.isEven());
}

/**
 * Description:     Test is even.
 * Expected value:  false
 */
TEST_F(BigNumberTest, Even3) {
    debug_logger->debug("======= Test Even3 - 1 =======");
    ASSERT_FALSE(number1.isEven());
}

/**
 * Description:     Test is even.
 * Expected value:  false
 */
TEST_F(BigNumberTest, Even4) {
    debug_logger->debug("======= Test Even4 - 11 =======");
    debug_logger->debug("------- Test Even4- set number1 to 11.");
    number1 = BigNumber("11");
    ASSERT_FALSE(number1.isEven());
}

//---------------------------------------------------------------------------------------------------------------IS EVEN

//-----------------------------------------------------------------------------------------------INCREMENT AND DECREMENT

/**
 * Description:     Test inc.
 * Expected value:  1 and 2
 */
TEST_F(BigNumberTest, Inc1) {
    debug_logger->debug("======= Test Inc1 - 1++ =======");
    ASSERT_EQ(BigNumber("1"), number1++);
    ASSERT_EQ(BigNumber("2"), number1);
}

/**
 * Description:     Test inc.
 * Expected value:  2
 */
TEST_F(BigNumberTest, Inc2) {
    debug_logger->debug("======= Test Inc2 - ++1 =======");
    ASSERT_EQ(BigNumber("2"), ++number1);
    ASSERT_EQ(BigNumber("2"), number1);
}

/**
 * Description:     Test dec.
 * Expected value:  1 and 0
 */
TEST_F(BigNumberTest, Dec1) {
    debug_logger->debug("======= Test Dec1 - 1-- =======");
    ASSERT_EQ(BigNumber("1"), number1--);
    ASSERT_EQ(BigNumber("0"), number1);
}

/**
 * Description:     Test dec.
 * Expected value:  0
 */
TEST_F(BigNumberTest, Dec2) {
    debug_logger->debug("======= Test Dec1 - --1 =======");
    ASSERT_EQ(BigNumber("0"), --number1);
    ASSERT_EQ(BigNumber("0"), number1);
}

//-----------------------------------------------------------------------------------------------INCREMENT AND DECREMENT

//--------------------------------------------------------------------------------------------------------CONVERT TO 256

/**
 * Description:     Test convert base2 to base256.
 * Expected value:
 */
TEST_F(BigNumberTest, Con1) {
    debug_logger->debug("======= Test Con1 - base2 =======");
    int baseFirst = 2;
    int baseSecond = 256;
    // 14578
    string n1 = "11100011110010";
    string n1Converted = "56:242";
    // 350
    string n2 = "101011110";
    string n2Converted = "1:94";

    number1.setNumber(n1);
    number1.setBase(baseFirst);
    number1.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n1Converted), number1);
    number1.setNumber(n1Converted);
    number1.setBase(baseSecond);
    number1.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n1, false), number1);

    number2.setNumber(n2);
    number2.setBase(baseFirst);
    number2.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n2Converted), number2);
    number2.setNumber(n2Converted);
    number2.setBase(baseSecond);
    number2.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n2, false), number2);
}

/**
 * Description:     Test convert base3 to base256.
 * Expected value:
 */
TEST_F(BigNumberTest, Con2) {
    debug_logger->debug("======= Test Con2 - base3 =======");
    int baseFirst = 3;
    int baseSecond = 256;
    // 14578
    string n1 = "201222221";
    string n1Converted = "56:242";
    // 350
    string n2 = "110222";
    string n2Converted = "1:94";

    number1.setNumber(n1);
    number1.setBase(baseFirst);
    number1.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n1Converted), number1);
    number1.setNumber(n1Converted);
    number1.setBase(baseSecond);
    number1.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n1, false), number1);

    number2.setNumber(n2);
    number2.setBase(baseFirst);
    number2.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n2Converted), number2);
    number2.setNumber(n2Converted);
    number2.setBase(baseSecond);
    number2.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n2, false), number2);
}

/**
 * Description:     Test convert base17 to base256.
 * Expected value:
 */
TEST_F(BigNumberTest, Con3) {
    debug_logger->debug("======= Test Con3 - base17 =======");
    int baseFirst = 17;
    int baseSecond = 256;
    // 14578
    string n1 = "2:16:7:9";
    string n1Converted = "56:242";
    // 350
    string n2 = "1:3:10";
    string n2Converted = "1:94";

    number1.setNumber(n1);
    number1.setBase(baseFirst);
    number1.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n1Converted), number1);
    number1.setNumber(n1Converted);
    number1.setBase(baseSecond);
    number1.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n1, false), number1);

    number2.setNumber(n2);
    number2.setBase(baseFirst);
    number2.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n2Converted), number2);
    number2.setNumber(n2Converted);
    number2.setBase(baseSecond);
    number2.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n2, false), number2);
}

/**
 * Description:     Test convert base10 to base256.
 * Expected value:
 */
TEST_F(BigNumberTest, Con4) {
    debug_logger->debug("======= Test Con4 - base10 =======");
    int baseFirst = 10;
    int baseSecond = 256;
    // 14578
    string n1 = "14578";
    string n1Converted = "56:242";
    // 350
    string n2 = "350";
    string n2Converted = "1:94";

    // 350
    string n3 = "79";
    string n3Converted = "79";

    number1.setNumber(n1);
    number1.setBase(baseFirst);
    number1.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n1Converted), number1);
    number1.setNumber(n1Converted);
    number1.setBase(baseSecond);
    number1.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n1, false), number1);

    number2.setNumber(n2);
    number2.setBase(baseFirst);
    number2.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n2Converted), number2);
    number2.setNumber(n2Converted);
    number2.setBase(baseSecond);
    number2.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n2, false), number2);

    number2.setNumber(n3);
    number2.setBase(baseFirst);
    number2.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n3Converted), number2);
    number2.setNumber(n3Converted);
    number2.setBase(baseSecond);
    number2.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n3, false), number2);
}

/**
 * Description:     Test convert base11 to base256.
 * Expected value:
 */
TEST_F(BigNumberTest, Con5) {
    debug_logger->debug("======= Test Con5 - base11 =======");
    int baseFirst = 11;
    int baseSecond = 256;
    // 14578
    string n1 = "10:10:5:3";
    string n1Converted = "56:242";
    // 350
    string n2 = "2:9:9";
    string n2Converted = "1:94";

    number1.setNumber(n1);
    number1.setBase(baseFirst);
    number1.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n1Converted), number1);
    number1.setNumber(n1Converted);
    number1.setBase(baseSecond);
    number1.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n1, false), number1);

    number2.setNumber(n2);
    number2.setBase(baseFirst);
    number2.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n2Converted), number2);
    number2.setNumber(n2Converted);
    number2.setBase(baseSecond);
    number2.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n2, false), number2);
}

/**
 * Description:     Test convert base21 to base256.
 * Expected value:
 */
TEST_F(BigNumberTest, Con6) {
    debug_logger->debug("======= Test Con6 - base21 =======");
    int baseFirst = 21;
    int baseSecond = 256;
    // 784521
    string n1 = "4:0:14:20:3";
    string n1Converted = "11:248:137";
    // 11111
    string n2 = "1:4:4:2";
    string n2Converted = "43:103";

    number1.setNumber(n1);
    number1.setBase(baseFirst);
    number1.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n1Converted), number1);
    number1.setNumber(n1Converted);
    number1.setBase(baseSecond);
    number1.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n1, false), number1);

    number2.setNumber(n2);
    number2.setBase(baseFirst);
    number2.convertFromTo(baseFirst, baseSecond);
    ASSERT_EQ(BigNumber(baseSecond, n2Converted), number2);
    number2.setNumber(n2Converted);
    number2.setBase(baseSecond);
    number2.convertFromTo(baseSecond, baseFirst);
    ASSERT_EQ(BigNumber(baseFirst, n2, false), number2);
}

//--------------------------------------------------------------------------------------------------------CONVERT TO 256

}
