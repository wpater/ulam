/*
 * BigNumber.cuh
 *
 *  Created on: 24 wrz 2017
 *      Author: wpater
 */

#ifndef INCLUDE_MODEL_BIGNUMBER_CUH_
#define INCLUDE_MODEL_BIGNUMBER_CUH_

#include <vector>
#include <string>
#include <algorithm>
#include <stdexcept>
#include "spdlog/spdlog.h"
#include <stdio.h>
#include <iostream>

using namespace std;

class BigNumber {

    /**
     * @brief Holds big number.
     */
    vector<unsigned char> number;
    /**
     * @brief Holds information about negative/positive.
     */
    bool positive;
    /**
     * @brief Holds information about primary base.
     */
    int base;
    /**
     * @brief Holds logger for debugging.
     */
    shared_ptr<spdlog::logger> debug_logger;

public:
    BigNumber(int, string, bool = true);

    /**
     * @brief Constructor with string.
     * @param n a string
     * @return a BigNumber object
     */
    BigNumber(string, bool = true);

    BigNumber();
    /**
     * @brief Constructor with vector.
     * @param base an int
     * @param n a vector
     * @return a BigNumber object
     */
    BigNumber(int, vector<unsigned char>, bool = true);

    /**
     * @brief Default destructor.
     */
    virtual ~BigNumber();

    /**
     * @brief Getter for vector with big number.
     * @return Pointer for vector<short>
     */
    vector<unsigned char> getNumber();

    /**
     * @brief Setter for vector with big number.
     * @param v is a vector<short>
     */
    void setNumber(vector<unsigned char>);

    /**
     * @brief Setter for vector with big number.
     * @param s a string
     */
    void setNumber(string);

    /**
     * @brief Getter for positive information.
     * @return boolean
     */
    bool getPositive();

    /**
     * @brief Setter for positive.
     * @param s a boolean
     */
    void setPositive(bool);

    /**
     * @brief Getter for base information.
     * @return int
     */
    int getBase();

    /**
     * @brief Setter for base.
     * @param s an int
     */
    void setBase(int);

    /**@brief To string method for vector
     * @return String
     */
    string toString();

    /**@brief Abs function for BigNumber
     * @return BigNumber
     */
    BigNumber abs();

    /**
     * @brief Adds two BigNumbers.
     * @param a BigNumber
     * @return Pointer to new BigNumber
     */
    BigNumber add(BigNumber);

    /**
     * @brief Subtracts two BigNumbers.
     * @param a BigNumber
     * @return Pointer to new BigNumber
     */
    BigNumber subtract(BigNumber);

    /**
     * @brief Multiplies two BigNumbers.
     * @param a BigNumber
     * @return Pointer to new BigNumber
     */
    BigNumber multiply(BigNumber);

    /**
     * @brief Divides two BigNumbers.
     * @param a BigNumber
     * @return Pointer to new BigNumber
     */
    BigNumber divide(BigNumber);

    /**
     * @brief Square of BigNumber.
     * @return Pointer to new BigNumber
     */
    BigNumber sqrt();

    /**
     * @brief Mod of BigNumber.
     * @return Pointer to new BigNumber
     */
    BigNumber mod(BigNumber);

    /**
     * @brief Checks that a BigNumber is even.
     * @return a boolean
     */
    bool isEven();

    void convertFromTo(int, int);

    /** @brief Comparator for BigNumber class.*/
    friend bool operator==(BigNumber b1, BigNumber b2) {
        if (!(b1.positive == b2.positive))
            return false;
        if (b1.number.size() != b2.number.size())
            return false;
        for (unsigned long i = 0; i < b1.number.size(); i++) {
            if (b1.number.at(i) != b2.number.at(i))
                return false;
        }
        return true;
    }

    /** @brief Comparator for BigNumber class.*/
    friend bool operator<(BigNumber b1, BigNumber b2) {
        if (!b1.positive && b2.positive)
            return true;
        if (b1.positive && !b2.positive)
            return false;
        if (b1.number.size() < b2.number.size())
            return true;
        if (b1.number.size() > b2.number.size())
            return false;
        for (unsigned long i = 0; i < b1.number.size(); i++) {
            if (b1.number.at(i) > b2.number.at(i))
                return false;
            else if (b1.number.at(i) < b2.number.at(i))
                return true;
        }
        return false;
    }

    /** @brief Comparator for BigNumber class.*/
    friend bool operator>(BigNumber b1, BigNumber b2) {
        if (!b1.positive && b2.positive)
            return false;
        if (b1.positive && !b2.positive)
            return true;
        if (b1.number.size() > b2.number.size())
            return true;
        if (b1.number.size() < b2.number.size())
            return false;
        for (unsigned long i = 0; i < b1.number.size(); i++) {
            if (b1.number.at(i) < b2.number.at(i))
                return false;
            else if (b1.number.at(i) > b2.number.at(i))
                return true;
        }
        return false;
    }

    /** @brief Comparator for BigNumber class.*/
    friend bool operator>=(BigNumber b1, BigNumber b2) {
        if (!b1.positive && b2.positive)
            return false;
        if (b1.positive && !b2.positive)
            return true;
        if (b1.number.size() > b2.number.size())
            return true;
        if (b1.number.size() < b2.number.size())
            return false;
        for (unsigned long i = 0; i < b1.number.size(); i++) {
            if (b1.number.at(i) < b2.number.at(i))
                return false;
            else if (b1.number.at(i) > b2.number.at(i))
                return true;
        }
        return true;
    }

    /** @brief Comparator for BigNumber class.*/
    friend bool operator<=(BigNumber b1, BigNumber b2) {
        if (!b1.positive && b2.positive)
            return true;
        if (b1.positive && !b2.positive)
            return false;
        if (b1.number.size() < b2.number.size())
            return true;
        if (b1.number.size() > b2.number.size())
            return false;
        for (unsigned long i = 0; i < b1.number.size(); i++) {
            if (b1.number.at(i) > b2.number.at(i))
                return false;
            else if (b1.number.at(i) < b2.number.at(i))
                return true;
        }
        return true;
    }

    /** @brief Minus for BigNumber class.*/
    BigNumber operator-() {
        BigNumber minus;
        minus.setNumber(number);
        minus.setPositive(!positive);
        return minus;
    }

    /** @brief Operator + for BigNumber class.*/
    friend BigNumber operator+(BigNumber b1, const BigNumber b2) {
        return b1.add(b2);
    }
    BigNumber operator+=(const BigNumber n) {
        *this = *this + n;
        return *this;
    }

    /** @brief Operator - for BigNumber class.*/
    friend BigNumber operator-(BigNumber b1, BigNumber b2) {
        return b1.subtract(b2);
    }
    BigNumber operator-=(const BigNumber n) {
        *this = *this - n;
        return *this;
    }

    /** @brief Operator * for BigNumber class.*/
    friend BigNumber operator*(BigNumber b1, BigNumber b2) {
        return b1.multiply(b2);
    }
    BigNumber operator*=(const BigNumber n) {
        *this = *this * n;
        return *this;
    }

    /** @brief Operator / for BigNumber class.*/
    friend BigNumber operator/(BigNumber b1, BigNumber b2) {
        return b1.divide(b2);
    }
    BigNumber operator/=(const BigNumber n) {
        *this = *this / n;
        return *this;
    }

    /** @brief Operator % for BigNumber class.*/
    friend BigNumber operator%(BigNumber b1, BigNumber b2) {
        return b1.mod(b2);
    }
    BigNumber operator%=(const BigNumber n) {
        *this = *this % n;
        return *this;
    }

    /** @brief Operator ++ for BigNumber class.*/
    BigNumber operator++() {
        *this = *this + BigNumber("1");
        return *this;
    }
    BigNumber operator++(int) {
        BigNumber tmp(*this);
        operator++();
        return tmp;
    }

    /** @brief Operator -- for BigNumber class.*/
    BigNumber operator--() {
        *this = *this - BigNumber("1");
        return *this;
    }
    BigNumber operator--(int) {
        BigNumber tmp(*this);
        operator--();
        return tmp;
    }
};

#endif /* INCLUDE_MODEL_BIGNUMBER_CUH_ */

/*! \class BigNumber BigNumber.cuh "include/model/BigNumber.cuh"
 *  \brief This is a class for representing big numbers.
 *
 * 	TODO: more specific description is needed
 */
