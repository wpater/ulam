/*
 * utility.cuh
 *
 *  Created on: 21 lut 2018
 *      Author: wpater
 */

#ifndef INCLUDE_ULAM_UTILITY_CUH_
#define INCLUDE_ULAM_UTILITY_CUH_

#include <fstream>
#include <string>
#include <iostream>

#include "model/BigNumber.cuh"
#include "model/Spiral.cuh"
#include "spdlog/spdlog.h"

namespace utility {

/**
 * Reads configuration from specified file.
 * @param filename a string with name of configuration file
 * @return a BigNumber from specified file
 */
BigNumber readConfiguration(string filename);

/**
 * Creates loggers for whole library. Use spdlog::get(logger_name) for getting logger.
 * Available loggers: "logger" and "debug_logger".
 */
void createLogger();

/**
 * Simple example with spiral. Prints spiral with size 43 and finds coordinates of number 27.
 */
void example();

/**
 * Simple example with spiral. Prints spiral with size 43 and finds coordinates of number 27.
 */
void exampleBigNumber();

/**
 * Convert vector of BigNumbers to string.
 * @param v a vector with BigNumbers
 * @return pretty print string
 */
string bigNumberVectorToString(vector<BigNumber> v);

}

#endif /* INCLUDE_ULAM_UTILITY_CUH_ */
