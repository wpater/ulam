#!/bin/bash

while :; do
	case "$1" in
		--help|-h)
			echo "Script for setting environment in Ulam project.
    Usage: source env.sh | ./env.sh [-h|--help]
        source env.sh will set all needed flags for nvcc.
        -h | --help    Print this help"
			exit 1
			;;
		*)
			break
			;;
	esac
	shift
done

echo "Setting flags ..."
echo "Adding /usr/local/cuda/lib to LD_LIBRARY_PATH"
export LD_LIBRARY_PATH=/usr/local/cuda/lib
echo "Adding /usr/local/cuda/bin to PATH"
export PATH=$PATH:/usr/local/cuda/bin

echo "Done"
