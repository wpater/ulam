/*
 * BigNumberCUDA.cuh
 *
 *  Created on: 2 lut 2018
 *      Author: wpater
 */

#ifndef INCLUDE_MODEL_BIGNUMBERCUDA_CUH_
#define INCLUDE_MODEL_BIGNUMBERCUDA_CUH_

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#define CUDA_FORCEINLINE __forceinline__
#else
#define CUDA_CALLABLE_MEMBER
#define CUDA_FORCEINLINE
#endif

#include <iostream>
#include <string>

template<size_t N> class BigNumberCUDA {

	int value;
	unsigned size = N;
	short * number;

public:
    CUDA_CALLABLE_MEMBER BigNumberCUDA() : value(0) {}
    CUDA_CALLABLE_MEMBER BigNumberCUDA(int v) : value(v) {}
    CUDA_CALLABLE_MEMBER BigNumberCUDA(short * n) : value(0), number(n) {}
    CUDA_CALLABLE_MEMBER BigNumberCUDA(int v, short * n) : value(v), number(n){
//    	number = new short[size];
//    	for (unsigned i = 0; i < size; i++) {
//    		number[i] = n[i];
//    	}
//    	number = n;
//    	value = v;
    }
    CUDA_CALLABLE_MEMBER ~BigNumberCUDA() {}

    CUDA_CALLABLE_MEMBER int getValue() {
    	return value;
    }

    CUDA_CALLABLE_MEMBER void setValue(int v) {
		value = v;
	}

    CUDA_CALLABLE_MEMBER unsigned getSize() {
		return size;
	}

    CUDA_CALLABLE_MEMBER short * getNumber() {
    	return number;
    }

    CUDA_CALLABLE_MEMBER void setNumber(short * n, unsigned s) {
    	number = (short *) malloc(sizeof(short) * s);
    	for (unsigned i = 0; i < s; i++) {
    		number[i] = n[i];
    	}
   	}

    CUDA_CALLABLE_MEMBER CUDA_FORCEINLINE int test() const {
    	return value + 10;
    }

    CUDA_CALLABLE_MEMBER CUDA_FORCEINLINE void add(BigNumberCUDA * const & bn) {
    	setValue(this->getValue() + bn->getValue());
    }

    CUDA_CALLABLE_MEMBER CUDA_FORCEINLINE void addNumber(BigNumberCUDA * const & bn) {
    	printf("Array before is: %d\n", size);
    	printf("Array before is: %d\n", this->getNumber()[0]);
    	printf("Array before is: %d\n", bn->getNumber()[0]);
//    	reverse(number, size);
//		reverse(bn->getNumber(), bn->getSize());
//		printf("Array after is: %d and %d \n", number[0], number[1]);
//		short * result = (short *) malloc(sizeof(short) * (size + bn->getSize()));//new short[size + bn->getSize()];
//		short inc = 0;
//		unsigned index = 0;
//		if (size >= bn->getSize()) {
//			for (unsigned int i = 0; i < bn->getSize(); i++) {
//				short sum = number[i] + bn->getNumber()[i] + inc;
//				inc = 0;
//				if (sum > 9) {
//					sum -= 10;
//					inc = 1;
//				}
//				printf("From device: %d ", sum);
//				result[index++] = sum;
//			}
//			for (unsigned int i = bn->getSize(); i < size; i++) {
//				short sum = number[i] + inc;
//				inc = 0;
//				if (sum > 9) {
//					sum -= 10;
//					inc = 1;
//				}
//				result[index++] = sum;
//			}
//			if (inc > 0) {
//				result[index++] = inc;
//			}
//		} else {
//			for (unsigned int i = 0; i < size; i++) {
//				short sum = number[i] + bn->getNumber()[i] + inc;
//				inc = 0;
//				if (sum > 9) {
//					sum -= 10;
//					inc = 1;
//				}
//				result[index++] = sum;
//			}
//			for (unsigned int i = size; i < bn->getSize(); i++) {
//				short sum = bn->getNumber()[i] + inc;
//				inc = 0;
//				if (sum > 9) {
//					sum -= 10;
//					inc = 1;
//				}
//				result[index++] = sum;
//			}
//			if (inc > 0) {
//				result[index++] = inc;
//			}
//		}
//		reverse(result, size + bn->getSize());
//		this->setNumber(result, size + bn->getSize());
	}

private:
    CUDA_CALLABLE_MEMBER CUDA_FORCEINLINE void reverse(short * array, unsigned s) {
    	short temp;
		for (unsigned i = 0; i < s / 2; i++) {
			temp = array[s - i - 1];
			array[s - i - 1] = array[i];
			array[i] = temp;
		}
	}
};



#endif /* INCLUDE_MODEL_BIGNUMBERCUDA_CUH_ */
