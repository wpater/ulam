/*
 * CPUTest.cu
 *
 *  Created on: 26 lut 2018
 *      Author: wpater
 */

#include <chrono>
#include <ctime>

#include "gtest/gtest.cuh"
#include "model/BigNumber.cuh"
#include "ulam/ulamCPU.cuh"

namespace {

class CPUTest: public ::testing::Test {

protected:

    std::shared_ptr<spdlog::logger> logger;

    void SetUp() {
        logger = spdlog::get("logger");
    }

    void TearDown() {
        logger->flush();
    }
};

//Disabled due small bandwidth in alg
///**
// * Description:     Resolve 15.
// * Expected value:  3 and 5
// */
//TEST_F(CPUTest, Resolve15) {
//    logger->info("Test 15 = 5 * 3");
//    set<BigNumber> expected;
//    BigNumber p("5");
//    BigNumber q("3");
//    expected.insert(p);
//    expected.insert(q);
//    BigNumber task = utility::readConfiguration("./config/easy/task_4.cfg");
//    auto start = std::chrono::system_clock::now();
//    ASSERT_EQ(expected, ulamcpu::resolve(task));
//    auto end = std::chrono::system_clock::now();
//    std::chrono::duration<double> elapsed_seconds = end - start;
//    logger->info("Resolved: {} and {} in {}s", p.toString(), q.toString(), elapsed_seconds.count());
//}

/**
 * Description:     Resolve 21.
 * Expected value:  3 and 7
 */
TEST_F(CPUTest, Resolve21) {
    logger->info("Test 21 = 7 * 3");
    set<BigNumber> expected;
    BigNumber p(10, "7", false);
    BigNumber q(10, "3", false);
    expected.insert(p);
    expected.insert(q);
    BigNumber task = utility::readConfiguration("./config/easy/task_5.cfg");
    auto start = std::chrono::system_clock::now();
    ASSERT_EQ(expected, ulamcpu::resolve(task));
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    logger->info("Resolved: {} and {} in {}s", p.toString(), q.toString(), elapsed_seconds.count());
}

// Disabled due small bandwidth in alg
///**
// * Description:     Resolve 33.
// * Expected value:  3 and 11
// */
//TEST_F(CPUTest, Resolve33) {
//    logger->info("Test 33 = 11 * 3");
//    set<BigNumber> expected;
//    BigNumber p("11");
//    BigNumber q("3");
//    expected.insert(p);
//    expected.insert(q);
//    BigNumber task = utility::readConfiguration("./config/easy/task_6.cfg");
//    auto start = std::chrono::system_clock::now();
//    ASSERT_EQ(expected, ulamcpu::resolve(task));
//    auto end = std::chrono::system_clock::now();
//    std::chrono::duration<double> elapsed_seconds = end - start;
//    logger->info("Resolved: {} and {} in {}s", p.toString(), q.toString(), elapsed_seconds.count());
//}

/**
 * Description:     Resolve 35.
 * Expected value:  7 and 5
 */
TEST_F(CPUTest, Resolve35) {
    logger->info("Test 35 = 5 * 7");
    set<BigNumber> expected;
    BigNumber p(10, "5", false);
    BigNumber q(10, "7", false);
    expected.insert(p);
    expected.insert(q);
    BigNumber task = utility::readConfiguration("./config/easy/task_6_2.cfg");
    auto start = std::chrono::system_clock::now();
    ASSERT_EQ(expected, ulamcpu::resolve(task));
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    logger->info("Resolved: {} and {} in {}s", p.toString(), q.toString(), elapsed_seconds.count());
}

/**
 * Description:     Resolve 767.
 * Expected value:  59 and 13
 */
TEST_F(CPUTest, Resolve767) {
    logger->info("Test 767 = 59 * 13");
    set<BigNumber> expected;
    BigNumber p(10, "59", false);
    BigNumber q(10, "13", false);
    expected.insert(p);
    expected.insert(q);
    BigNumber task = utility::readConfiguration("./config/medium/task_10.cfg");
    auto start = std::chrono::system_clock::now();
    ASSERT_EQ(expected, ulamcpu::resolve(task));
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    logger->info("Resolved: {} and {} in {}s", p.toString(), q.toString(), elapsed_seconds.count());
}

/**
 * Description:     Resolve 1763.
 * Expected value:  41 and 43
 */
TEST_F(CPUTest, Resolve1763) {
    logger->info("Test 1763 = 41 * 43");
    set<BigNumber> expected;
    BigNumber p(10, "41", false);
    BigNumber q(10, "43", false);
    expected.insert(p);
    expected.insert(q);
    BigNumber task = utility::readConfiguration("./config/medium/task_11.cfg");
    auto start = std::chrono::system_clock::now();
    ASSERT_EQ(expected, ulamcpu::resolve(task));
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    logger->info("Resolved: {} and {} in {}s", p.toString(), q.toString(), elapsed_seconds.count());
}

/**
 * Description:     Resolve 7663.
 * Expected value:  97 and 79
 */
TEST_F(CPUTest, Resolve7663) {
    logger->info("Test 7663 = 97 * 79");
    set<BigNumber> expected;
    BigNumber p(10, "79", false);
    BigNumber q(10, "97", false);
    expected.insert(p);
    expected.insert(q);
    BigNumber task = utility::readConfiguration("./config/medium/task_13.cfg");
    auto start = std::chrono::system_clock::now();
    ASSERT_EQ(expected, ulamcpu::resolve(task));
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    logger->info("Resolved: {} and {} in {}s", p.toString(), q.toString(), elapsed_seconds.count());
}

/**
 * Description:     Resolve 158299.
 * Expected value:  311 and 509
 */
TEST_F(CPUTest, Resolve158299) {
    logger->info("Test 158299 = 311 * 509");
    set<BigNumber> expected;
    BigNumber p(10, "311", false);
    BigNumber q(10, "509", false);
    expected.insert(p);
    expected.insert(q);
    BigNumber task = utility::readConfiguration("./config/hard/task_18.cfg");
    auto start = std::chrono::system_clock::now();
    ASSERT_EQ(expected, ulamcpu::resolve(task));
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    logger->info("Resolved: {} and {} in {}s", p.toString(), q.toString(), elapsed_seconds.count());
}

/**
 * Description:     Resolve 494321.
 * Expected value:  757 and 653
 */
TEST_F(CPUTest, Resolve494321) {
    logger->info("Test 494321 = 757 * 653");
    set<BigNumber> expected;
    BigNumber p(10, "757", false);
    BigNumber q(10, "653", false);
    expected.insert(p);
    expected.insert(q);
    BigNumber task = utility::readConfiguration("./config/hard/task_19.cfg");
    auto start = std::chrono::system_clock::now();
    ASSERT_EQ(expected, ulamcpu::resolve(task));
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    logger->info("Resolved: {} and {} in {}s", p.toString(), q.toString(), elapsed_seconds.count());
}

/**
 * Description:     Resolve 1197397.
 * Expected value:  997 and 1201
 */
TEST_F(CPUTest, Resolve1197397) {
    logger->info("Test 1197397 = 997 * 1201");
    set<BigNumber> expected;
    BigNumber p(10, "1201", false);
    BigNumber q(10, "997", false);
    expected.insert(p);
    expected.insert(q);
    BigNumber task = utility::readConfiguration("./config/hard/task_21.cfg");
    auto start = std::chrono::system_clock::now();
    ASSERT_EQ(expected, ulamcpu::resolve(task));
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    logger->info("Resolved: {} and {} in {}s", p.toString(), q.toString(), elapsed_seconds.count());
}

/**
 * Description:     Resolve 13751921.
 * Expected value:  3571 and 3851
 */
TEST_F(CPUTest, Resolve13751921) {
    logger->info("Test 13751921 = 3571 * 3851");
    set<BigNumber> expected;
    BigNumber p(10, "3571", false);
    BigNumber q(10, "3851", false);
    expected.insert(p);
    expected.insert(q);
    BigNumber task = utility::readConfiguration("./config/hard/task_24.cfg");
    auto start = std::chrono::system_clock::now();
    ASSERT_EQ(expected, ulamcpu::resolve(task));
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    logger->info("Resolved: {} and {} in {}s", p.toString(), q.toString(), elapsed_seconds.count());
}

}
