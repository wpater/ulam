/*
 * ulamCPU.cu
 *
 *  Created on: 22 lut 2018
 *      Author: wpater
 */

#include "ulam/ulamCPU.cuh"

namespace ulamcpu {

vector<BigNumber> diagonals(BigNumber task, BigNumber start) {
    vector<BigNumber> numbers;
    Spiral spiral = Spiral();
    Point taskPoint = spiral.getPoint(start), nextPoint = Point();
    BigNumber inc(10, "1");
    BigNumber next, prev;
    if (start <= task)
        numbers.push_back(start);
    // x += 1, y += 1
    BigNumber x = taskPoint.getX() + inc, y = taskPoint.getY() + inc;
    nextPoint.setX(x);
    nextPoint.setY(y);
    next = spiral.getNumber(nextPoint), prev = next;
    while (prev >= next || next < task) {
        if (next <= task)
            numbers.push_back(next);
        x++;
        y++;
        nextPoint.setX(x);
        nextPoint.setY(y);
        prev = next;
        next = spiral.getNumber(nextPoint);
    }
    // x += 1, y -= 1
    x = taskPoint.getX() + inc;
    y = taskPoint.getY() - inc;
    nextPoint.setX(x);
    nextPoint.setY(y);
    next = spiral.getNumber(nextPoint), prev = next;
    while (prev >= next || next < task) {
        if (next <= task)
            numbers.push_back(next);
        x++;
        y--;
        nextPoint.setX(x);
        nextPoint.setY(y);
        prev = next;
        next = spiral.getNumber(nextPoint);
    }
    // x -= 1, y -= 1
    x = taskPoint.getX() - inc;
    y = taskPoint.getY() - inc;
    nextPoint.setX(x);
    nextPoint.setY(y);
    next = spiral.getNumber(nextPoint), prev = next;
    while (prev >= next || next < task) {
        if (next <= task)
            numbers.push_back(next);
        x--;
        y--;
        nextPoint.setX(x);
        nextPoint.setY(y);
        prev = next;
        next = spiral.getNumber(nextPoint);
    }
    // x -= 1, y += 1
    x = taskPoint.getX() - inc;
    y = taskPoint.getY() + inc;
    nextPoint.setX(x);
    nextPoint.setY(y);
    next = spiral.getNumber(nextPoint), prev = next;
    while (prev >= next || next < task) {
        if (next <= task)
            numbers.push_back(next);
        x--;
        y++;
        nextPoint.setX(x);
        nextPoint.setY(y);
        prev = next;
        next = spiral.getNumber(nextPoint);
    }
    return numbers;
}

bool couldBePrime(BigNumber number) {
    BigNumber one(10, "1");
    BigNumber five(10, "5");
    BigNumber six(10, "6");
    BigNumber mod = number % six;
    if (mod == one || mod == five)
        return true;
    return false;
}

vector<BigNumber> possiblePrimes(vector<BigNumber> v) {
    BigNumber one(10, "1");
    vector<BigNumber> couldBePrimes;
    for (BigNumber n : v) {
        if (couldBePrime(n) && !(one == n))
            couldBePrimes.push_back(n);
    }
    return couldBePrimes;
}

bool isDivider(BigNumber number, BigNumber candidate) {
    BigNumber zero = BigNumber();
    if (number % candidate == zero)
        return true;
    return false;
}

vector<BigNumber> findDividers(vector<BigNumber> v, BigNumber number) {
    vector<BigNumber> dividers;
    for (BigNumber n : v) {
        if (isDivider(number, n))
            dividers.push_back(n);
    }
    return dividers;
}

bool isPrime(BigNumber n) {
    BigNumber b(10, "2");
    BigNumber inc(10, "1");
    BigNumber zero = BigNumber();
    BigNumber limit = n.sqrt().add(inc);
    for (; b < limit; b++) {
        if (zero == n % b)
            return false;
    }
    return true;
}

vector<BigNumber> findPrimes(vector<BigNumber> v) {
    vector<BigNumber> primes;
    for (BigNumber n : v) {
        if (isPrime(n))
            primes.push_back(n);
    }
    return primes;
}

set<BigNumber> resolve(BigNumber task) {
    std::shared_ptr<spdlog::logger> debug_logger = spdlog::get("debug_logger");
    std::shared_ptr<spdlog::logger> logger = spdlog::get("logger");
    debug_logger->debug("Resolve: start.");
    auto start = std::chrono::system_clock::now();
    set<BigNumber> solution;
    int nextUp = 0;
    int nextDown = 0;
    int decimal = 10;
    bool started = false;
    BigNumber sqrt = task.sqrt();
    BigNumber additional = sqrt.divide(BigNumber(decimal, "4")).multiply(BigNumber(decimal, "3"));
    BigNumber sqrt_plus_additional = sqrt.add(additional);
    BigNumber sqrt_minus_additional = sqrt.subtract(additional);
    if (sqrt.isEven()) {
        sqrt++;
    }
    Spiral spiral = Spiral();
    Point mid = spiral.getPoint(sqrt);
    BigNumber minus = BigNumber(decimal, "-1");
    std::cout << "Start in : " << sqrt.toString() << "\n";
    for (unsigned i = 0; nextUp + nextDown < 2; i++) {
        std::cout << "Iteration: " << i << "\n";
        debug_logger->debug("Iteration {}", i);
        logger->info("Iteration {}", i);
        BigNumber next = BigNumber(decimal, std::to_string((i * 2)));
        if (i % 2 == 1)
            next *= minus;
        if (next.getPositive() && nextUp == 1)
            continue;
        else if (!next.getPositive() && nextDown == 1)
            continue;

        mid.setY(mid.getY() + next);
        BigNumber midNumber = spiral.getNumber(mid);
        if (midNumber.isEven())
            break;
        vector<BigNumber> numbers = ulamcpu::diagonals(sqrt_plus_additional, midNumber);
        vector<BigNumber> numbersToCheck;
        for (BigNumber n : numbers) {
            if (n <= sqrt_plus_additional && n >= sqrt_minus_additional)
                numbersToCheck.push_back(n);
        }
        std::cout << "Vector to check: " << utility::bigNumberVectorToString(numbersToCheck) << "\n\n";
        debug_logger->debug("Vector to check: {}", utility::bigNumberVectorToString(numbersToCheck));
        logger->info("Vector to check: {}", utility::bigNumberVectorToString(numbersToCheck));
        if (numbersToCheck.size() == 0) {
            if (started) {
                if (next.getPositive())
                    nextUp++;
                else
                    nextDown++;
            }
        } else {
            started = true;
            if (next.getPositive())
                nextUp = 0;
            else
                nextDown = 0;
        }

        // Create vector with numbers which could be prime
        if (numbersToCheck.size() > 0) {
            vector<BigNumber> couldBePrimes = ulamcpu::possiblePrimes(numbersToCheck);
            std::cout << "Possible primes: " << utility::bigNumberVectorToString(couldBePrimes) << "\n\n";
            debug_logger->debug("Possible primes: {}", utility::bigNumberVectorToString(couldBePrimes));
            logger->info("Possible primes: {}", utility::bigNumberVectorToString(couldBePrimes));
            // If not empty - check witch of them are dividers of task
            if (couldBePrimes.size() > 0) {
                vector<BigNumber> dividers = ulamcpu::findDividers(couldBePrimes, task);
                std::cout << "Dividers: " << utility::bigNumberVectorToString(dividers) << "\n\n";
                debug_logger->debug("Dividers: {}", utility::bigNumberVectorToString(dividers));
                logger->info("Dividers: {}", utility::bigNumberVectorToString(dividers));
                // If not empty check witch of them are primes
                if (dividers.size() > 0) {
                    vector<BigNumber> primes = ulamcpu::findPrimes(dividers);
                    std::cout << "Primes: " << utility::bigNumberVectorToString(primes) << "\n\n";
                    debug_logger->debug("Primes: {}", utility::bigNumberVectorToString(primes));
                    logger->info("Primes: {}", utility::bigNumberVectorToString(primes));
                    if (primes.size() > 0) {
                        BigNumber p = primes[0];
                        BigNumber q;
                        if (primes.size() == 1)
                            q = task.divide(p);
                        else
                            q = primes[1];
                        auto end = std::chrono::system_clock::now();
                        if (task.getBase() != 10) {
                            p.convertFromTo(p.getBase(), decimal);
                            q.convertFromTo(q.getBase(), decimal);
                            task.convertFromTo(task.getBase(), decimal);
                        }
                        std::chrono::duration<double> elapsed_seconds = end - start;
                        std::cout << "Solution for " << task.toString() << " is " << p.toString() << " and "
                                << q.toString() << ". " << i << " iterations and " << elapsed_seconds.count()
                                << " seconds" << "\n\n";
                        debug_logger->debug("Solution: {} = {} * {} in {} iterations and in {}s", task.toString(),
                                p.toString(), q.toString(), i, elapsed_seconds.count());
                        logger->info("Solution: {} = {} * {} in {} iterations and in {}s", task.toString(),
                                p.toString(), q.toString(), i, elapsed_seconds.count());
                        nextUp = 2; // stop
                        solution.insert(p);
                        solution.insert(q);
                    }
                }
            }
        }
    }
    debug_logger->debug("Resolve: end.");
    return solution;
}

}
