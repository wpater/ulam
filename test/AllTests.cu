/*
 * AllTests.cpp
 *
 *  Created on: 14 sie 2017
 *      Author: wpater
 */

#include <stdio.h>
#include <iostream>

#include "gtest/gtest.cuh"
#include "spdlog/spdlog.h"
#include "ulam/utility.cuh"

using namespace std;

int main(int argc, char **argv) {
    try {
        utility::createLogger();
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    } catch (const spdlog::spdlog_ex& ex) {
        std::cout << "Log initialization failed: " << ex.what() << std::endl;
    }
}
