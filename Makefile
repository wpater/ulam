NVCC = nvcc
LIB :=
OPT = --compiler-options
# -v options is --verbose
NVCC_OPTIONS = -std=c++11 -D_MWAITXINTRIN_H_INCLUDED #-g #-dc#-v
# Do not use -pedantic options for CUDA project - many useless warnings
G++_OPTIONS = $(OPT) -Wall $(OPT) -Wshadow $(OPT) -Wextra $(OPT) -pthread $(OPT) -march=native -Wno-deprecated-gpu-targets

# Target
TARGET := ulam
TEST_TARGET := test
DOC_TARGET := doc

# Directories
SRCDIR := src
BUILDDIR := build
BINDIR := bin
TESTDIR := test
LOGSDIR := logs
DOCDIR := html latex

# Header files
INC := -I include

# Files
SRC := $(shell find ./$(SRCDIR) -name "*.cu")
OBJECTS := $(patsubst ./$(SRCDIR)/%, ./$(BUILDDIR)/%, $(SRC:.cu=.o))
TEST_SRC := $(shell find ./$(TESTDIR) -name "*.cu")
TESTS := $(shell echo $(OBJECTS) | sed 's/\.\/build\/main.o//g')

# Targets
all: $(TARGET) $(TEST_TARGET)

# Linking
$(TARGET): $(OBJECTS)
	@echo "Linking..."
	@mkdir -p $(BINDIR)
	$(NVCC) $(NVCC_OPTIONS) $(G++_OPTIONS) $^ -o $(BINDIR)/$(TARGET) $(LIB)

# Building
$(BUILDDIR)/%.o: $(SRCDIR)/%.cu
	@mkdir -p $(dir $@)
	$(NVCC) $(NVCC_OPTIONS) $(G++_OPTIONS) $(INC) -c -o $@ $<

# Testing
$(TEST_TARGET): $(TESTS)
	@echo "Building tests..."
	@mkdir -p $(BINDIR)
	$(NVCC) $(NVCC_OPTIONS) $(G++_OPTIONS) $(TEST_SRC) $^ -o $(BINDIR)/test $(LIB) $(INC)

# Documentation
$(DOC_TARGET):
	@echo " rm -rf $(DOCDIR)" ; rm -rf $(DOCDIR)
	doxygen config/doxygen.conf

# Cleaning
clean:
	@echo "Cleaning..."
	@echo " rm -rf $(BUILDDIR) $(BINDIR) $(TARGET) $(DOCDIR)" ; rm -rf $(BUILDDIR) $(BINDIR) $(TARGET) $(DOCDIR)
	
# Removing logs
rlog:
	@echo "Removing logs..."
	@echo " rm -rf $(LOGSDIR)/*" ; rm -rf $(LOGSDIR)/*

.PHONY: clean