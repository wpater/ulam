/*
 * BigNumber.cu
 *
 *  Created on: 24 wrz 2017
 *      Author: wpater
 */

#include "model/BigNumber.cuh"

BigNumber::BigNumber() : BigNumber(256, "0", false) {}

BigNumber::BigNumber(string n, bool convert) : BigNumber(10, n, convert) {}

BigNumber::BigNumber(int b, vector<unsigned char> v, bool convert) {
    debug_logger = spdlog::get("debug_logger");
    setPositive(true);
    setBase(b);
    setNumber(v);
    if (convert && b != 256)
        convertFromTo(b, 256);
}

BigNumber::BigNumber(int b, string n, bool convert) {
    debug_logger = spdlog::get("debug_logger");
    setPositive(true);
    setBase(b);
    setNumber(n);
    if (convert && b != 256)
        convertFromTo(b, 256);
}

BigNumber::~BigNumber() {
    // TODO Auto-generated destructor stub
}

void BigNumber::setNumber(string s) {
    vector<unsigned char> newNumber;
    std::string delimiter = ":";
    std::size_t found = s.find(delimiter);
    if (found != std::string::npos) {
        size_t pos = 0;
        std::string token;
        while ((pos = s.find(delimiter)) != std::string::npos) {
            token = s.substr(0, pos);
            if (token == "-")
                setPositive(false);
            else
                newNumber.push_back(std::stoi(token));
            s.erase(0, pos + delimiter.length());
        }
        newNumber.push_back(std::stoi(s));
    } else {
        // Case for positive number with one digit in base bigger than 10 - it doesn't contains ':'.
        if (s.find("-") == std::string::npos && 1 < s.size() && s.size() <= 3
                && 0 < std::stoi(s) && std::stoi(s) < this->base)
            newNumber.push_back(std::stoi(s));
        else
            for (char c : s) {
                if (c == '-')
                    setPositive(false);
                else
                    newNumber.push_back((unsigned char) strtoul(&c, NULL, 0));
            }
    }
    number = newNumber;
}

// TODO: add documentation
BigNumber BigNumber::add(BigNumber big) {
    debug_logger->debug("Add start.");
    BigNumber newNumber(this->base, "0", false);
    // Support for minus
    if (!positive && !big.getPositive()) {
        debug_logger->debug("Both negatives - sum and set minus.");
        positive = true;
        big.setPositive(true);
        newNumber = this->add(big);
        newNumber.setPositive(false);
        positive = false;
        big.setPositive(false);
    } else if (positive && !big.getPositive()) {
        debug_logger->debug("First positive, second negative.");
        if (*this == big.abs()) {
            debug_logger->debug("First = |Second| -> result 0.");
        } else if (*this > big.abs()) {
            debug_logger->debug("First > |Second| -> first subtract |second|.");
            newNumber = this->subtract(big.abs());
        } else {
            debug_logger->debug("First < |Second| -> |second| subtract first and set minus.");
            newNumber = big.abs().subtract(*this);
            newNumber.setPositive(false);
        }
    } else if (!positive && big.getPositive()) {
        debug_logger->debug("First negative, second positive.");
        if (this->abs() == big) {
            debug_logger->debug("|First| = Second -> result 0.");
            newNumber = BigNumber();
        } else if (this->abs() < big) {
            debug_logger->debug("|First| < Second -> second subtract |first|.");
            newNumber = big.subtract(this->abs());
        } else {
            debug_logger->debug("|First| > Second -> |first| subtract second and set minus.");
            newNumber = this->abs().subtract(big);
            newNumber.setPositive(false);
        }
        // Adding two positives
    } else {
        vector<unsigned char> reversed_first = number;
        reverse(reversed_first.begin(), reversed_first.end());
        vector<unsigned char> reversed_second = big.getNumber();
        reverse(reversed_second.begin(), reversed_second.end());
        vector<unsigned char> result;
        char inc = 0;
        if (reversed_first.size() >= reversed_second.size()) {
            for (unsigned int i = 0; i < reversed_second.size(); i++) {
                debug_logger->debug("Adding {} and {}. Inc is {}.", (int) reversed_first.at(i),
                        (int) reversed_second.at(i), (int) inc);
                int sum = reversed_first.at(i) + reversed_second.at(i) + inc;
                inc = 0;
                if (sum > this->base - 1) {
                    sum -= this->base;
                    inc = 1;
                }
                result.push_back(sum);
            }
            for (unsigned int i = reversed_second.size(); i < reversed_first.size(); i++) {
                int sum = reversed_first.at(i) + inc;
                inc = 0;
                if (sum > this->base - 1) {
                    sum -= this->base;
                    inc = 1;
                }
                result.push_back(sum);
            }
            if (inc > 0) {
                result.push_back(inc);
            }
        } else {
            for (unsigned int i = 0; i < reversed_first.size(); i++) {
                debug_logger->debug("Adding {} and {}. Inc is {}.", (int) reversed_first.at(i),
                        (int) reversed_second.at(i), (int) inc);
                int sum = reversed_first.at(i) + reversed_second.at(i) + inc;
                inc = 0;
                if (sum > this->base - 1) {
                    sum -= this->base;
                    inc = 1;
                }
                result.push_back(sum);
            }
            for (unsigned int i = reversed_first.size(); i < reversed_second.size(); i++) {
                int sum = reversed_second.at(i) + inc;
                inc = 0;
                if (sum > this->base - 1) {
                    sum -= this->base;
                    inc = 1;
                }
                result.push_back(sum);
            }
            if (inc > 0) {
                result.push_back(inc);
            }
        }
        reverse(result.begin(), result.end());
        newNumber = BigNumber(this->base, result, false);
    }
    debug_logger->debug("Add end.");
    return newNumber;
}

BigNumber BigNumber::subtract(BigNumber big) {
    debug_logger->debug("Sub start.");
    BigNumber newNumber(this->base, "0", false);
    // Support for minus
    if (!positive && !big.getPositive()) {
        debug_logger->debug("Both negatives - |first| - |second| and set minus.");
        newNumber = this->abs().subtract(big.abs());
        newNumber.setPositive(false == newNumber.getPositive());
    } else if (positive && !big.getPositive()) {
        debug_logger->debug("First positive and second negative - first + |second|.");
        newNumber = this->add(big.abs());
    } else if (!positive && big.getPositive()) {
        debug_logger->debug("First negative and second positive - |first| + second and set minus.");
        newNumber = this->abs().add(big);
        newNumber.setPositive(false);
        // Subtracting two positives
    } else {
        vector<unsigned char> reversed_first, reversed_second;
        bool minus = false;
        if (*this >= big) {
            reversed_first = number;
            reverse(reversed_first.begin(), reversed_first.end());
            reversed_second = big.getNumber();
            reverse(reversed_second.begin(), reversed_second.end());
        } else {
            reversed_first = big.getNumber();
            reverse(reversed_first.begin(), reversed_first.end());
            reversed_second = number;
            reverse(reversed_second.begin(), reversed_second.end());
            minus = true;
        }
        vector<unsigned char> result;
        char dec = 0;
        for (unsigned int i = 0; i < reversed_second.size(); i++) {
            debug_logger->debug("Subtracting {} and {}. Dec is {}.", (int) reversed_first.at(i),
                    (int) reversed_second.at(i), (int) dec);
            int sum = reversed_first.at(i) - reversed_second.at(i) + dec;
            dec = 0;
            if (sum < 0 && !(i + 1 == reversed_second.size() && reversed_first.size() == reversed_second.size())) {
                sum += this->base;
                dec = -1;
            }
            if (dec != 0 || sum != 0 || i < reversed_second.size() || result.size() == 0) {
                if (sum >= 0)
                    result.push_back(sum);
                else { // This case happens only if this is last iteration.
                    minus = true;
                    result.push_back(-sum);
                }
            }
        }
        for (unsigned int i = reversed_second.size(); i < reversed_first.size(); i++) {
            int sum = reversed_first.at(i) + dec;
            dec = 0;
            if (sum < 0) {
                sum += this->base;
                dec = -1;
            }
            result.push_back(sum);
        }
        if (dec < 0) {
            result.push_back(dec);
        }
        // remove zeros
        while (result.back() == 0 && result.size() > 1)
            result.pop_back();
        reverse(result.begin(), result.end());
        newNumber = BigNumber(this->base, result, false);
        if (minus)
            newNumber.setPositive(false);
    }
    debug_logger->debug("Sub end.");
    return newNumber;
}

BigNumber BigNumber::multiply(BigNumber big) {
    debug_logger->debug("Multiply start.");
    BigNumber result(this->base, "0", false);
    if (big == result || *this == result)
        return result;
    BigNumber inc(this->base, "1", false);
    BigNumber this_abs = this->abs();
    BigNumber big_abs = big.abs();
    BigNumber b = BigNumber(this->base, "0", false);
    if (*this < big) {
        for (; b < this_abs; b = b.add(inc)) {
            debug_logger->debug("Iteration number: {}.", b.toString());
            result = result.add(big_abs);
        }
    } else {
        for (; b < big_abs; b = b.add(inc)) {
            debug_logger->debug("Iteration number: {}.", b.toString());
            result = result.add(this_abs);
        }
    }
    result.setPositive(this->positive == big.getPositive());
    debug_logger->debug("Multiply end.");
    return result;
}

BigNumber BigNumber::divide(BigNumber big) {
    debug_logger->debug("Divide start.");
    BigNumber result(this->base, "0", false);
    if (big == result)
        throw std::invalid_argument("Dividing by zero");
    if (*this == result)
        return result;
    BigNumber this_abs = this->abs();
    BigNumber big_abs = big.abs();
    BigNumber inc(this->base, "1", false);
    while (this_abs >= big_abs) {
        debug_logger->debug("Divide mid: {} {}.", result.toString(), this_abs.toString());
        result = result.add(inc);
        this_abs = this_abs.subtract(big_abs);
    }
    result.setPositive(this->positive == big.getPositive());
    debug_logger->debug("Divide end.");
    return result;
}

/**
 * It's special version of sqrt - floor from real sqrt.
 * @return
 */
BigNumber BigNumber::sqrt() {
    BigNumber result = BigNumber(this->base, "0", false), two(this->base, "2", false);
    BigNumber half = this->divide(two), power;
    BigNumber inc("1"); //, * temp;
    do {
        result = result.add(inc);
        power = result.multiply(result);
    } while (power < *this);

    BigNumber p1 = result.multiply(result), p2 = result.subtract(inc).multiply(result.subtract(inc));
    if (p1 > *this)
        result = result.subtract(inc);
    debug_logger->debug("Result for {} is: {}.", this->toString(), result.toString());
    result.setPositive(true);
    return result;
}

BigNumber BigNumber::mod(BigNumber big) {
    BigNumber temp, result = this->subtract(big);
    if (big == BigNumber(this->base, "0", false)) {
        throw std::invalid_argument("Dividing by zero");
    }
    if (*this == result)
        return result;
    if (*this < big)
        return *this;
    while (big <= result) {
        temp = result;
        result = result.subtract(big);
    }
    debug_logger->debug("Result for {} mod {} is: {}.", this->toString(), big.toString(), result.toString());
    return result;
}

void BigNumber::setNumber(vector<unsigned char> v) {
    number = v;
}

vector<unsigned char> BigNumber::getNumber() {
    return number;
}

void BigNumber::setPositive(bool p) {
    positive = p;
}

bool BigNumber::getPositive() {
    return positive;
}

void BigNumber::setBase(int b) {
    base = b;
}

int BigNumber::getBase() {
    return base;
}

string BigNumber::toString() {
    string s = "";
    if (!positive)
        s.append("-:");
    for (unsigned char digit : number) {
        s.append(to_string(digit));
        s.append(":");
    }
    s.pop_back();
    return s;
}

BigNumber BigNumber::abs() {
    BigNumber result = BigNumber(this->base, this->getNumber(), false);
    result.setPositive(true);
    return result;
}

bool BigNumber::isEven() {
    string s = to_string(this->number[this->number.size() - 1]);
    if (BigNumber(this->base, s, false).mod(BigNumber(this->base, "2", false)) == BigNumber(this->base, "0", false))
        return true;
    return false;
}

void BigNumber::convertFromTo(int from, int to) {
    // Convert divider to base 'from'.
    int decimalBase = 10;
    bool plus = this->getPositive();
    this->setPositive(true);
    string n = this->toString();
    BigNumber baseFrom = BigNumber(decimalBase, std::to_string(from), false);
    BigNumber baseTo = BigNumber(decimalBase, std::to_string(to), false);
    unsigned char digit;
    int index;
    vector<unsigned char> v;
    while (baseTo >= baseFrom) {
        digit = 0;
        index = 0;
        BigNumber r = baseTo.mod(baseFrom);
        vector<unsigned char> rNumber = r.getNumber();
        reverse(rNumber.begin(), rNumber.end());
        for (unsigned char d : rNumber) {
            digit += std::pow(decimalBase, index++) * d;
        }
        v.push_back(digit);
        baseTo /= baseFrom;
    }
    digit = 0;
    index = 0;
    vector<unsigned char> lastRNumber = baseTo.getNumber();
    reverse(lastRNumber.begin(), lastRNumber.end());
    for (unsigned char d : lastRNumber) {
        digit += std::pow(decimalBase, index++) * d;
    }
    v.push_back(digit);
    reverse(v.begin(), v.end());
    BigNumber dividerInPrimaryBase = BigNumber(from, v, false); // It's new base in old one.
    BigNumber primaryNumber = BigNumber(from, n, false); // It's number n in base 'from'.
    // Divide given number n by divider (both in base 'from') and collect rests in vector.
    vector<unsigned char> converted;
    while (primaryNumber >= dividerInPrimaryBase) {
        digit = 0;
        index = 0;
        BigNumber r = primaryNumber.mod(dividerInPrimaryBase);
        // r is in base b so convert it to base 'to'.
        vector<unsigned char> rNumber = r.getNumber();
        reverse(rNumber.begin(), rNumber.end());
        for (unsigned char d : rNumber) {
            digit += std::pow(from, index++) * d;
        }
        // Save converted number in vector.
        converted.push_back(digit);
        primaryNumber /= dividerInPrimaryBase;
    }
    digit = 0;
    index = 0;
    lastRNumber = primaryNumber.getNumber();
    reverse(lastRNumber.begin(), lastRNumber.end());
    for (unsigned char d : lastRNumber) {
        digit += std::pow(from, index++) * d;
    }
    converted.push_back(digit);
    reverse(converted.begin(), converted.end());
    this->setNumber(converted);
    this->setBase(to);
    this->setPositive(plus);
    debug_logger->debug("{} in base{} converted to {} in base{}.", n, from, this->toString(), to);
}

