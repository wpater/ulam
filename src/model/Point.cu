/*
 * Point.cpp
 *
 *  Created on: 12 sie 2017
 *      Author: wpater
 */

#include "model/Point.cuh"

Point::Point() :
        x(BigNumber()), y(BigNumber()) {
}

Point::Point(BigNumber xx, BigNumber yy) :
        x(xx), y(yy) {
}

Point::~Point() {
    // TODO Auto-generated destructor stub
}

BigNumber Point::getX() {
    return x;
}

void Point::setX(BigNumber a) {
    x = a;
}

BigNumber Point::getY() {
    return y;
}

void Point::setY(BigNumber a) {
    y = a;
}

string Point::toString() {
    string s = "Point(";
    s.append(x.toString());
    s.append(", ");
    s.append(y.toString());
    s.append(")");
    return s;
}
