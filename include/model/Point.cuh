/*
 * Point.h
 *
 *  Created on: 12 sie 2017
 *      Author: wpater
 */

#ifndef POINT_H_
#define POINT_H_

#include "model/BigNumber.cuh"

class Point {

    /** @brief Holds x coordinate.*/
    BigNumber x;
    /** @brief Holds y coordinate.*/
    BigNumber y;

public:
    /** @brief Default constructor.
     *  @return a Point class with `x` = 0 and `y` = 0. */
    Point();

    /** @brief Point constructor.
     *  @param x a BigNumber
     *  @param y a BigNumber
     *  @return a Point class with specified coordinates. */
    Point(BigNumber, BigNumber);

    /** @brief Default destructor. */
    virtual ~Point();

    /** @brief Returns x coordinate.
     *  @return a BigNumber */
    BigNumber getX();

    /**	@brief Sets x coordinate.
     *  @param a a BigNumberr */
    void setX(BigNumber);

    /** @brief Returns y coordinate.
     *  @return a BigNumber */
    BigNumber getY();

    /** @brief Sets y coordinate.
     *  @param a a BigNumber */
    void setY(BigNumber);

    /**@brief To string method for Point
     * @return String
     */
    string toString();

    /** @brief Comparator for Point class.*/
    friend bool operator==(Point p1, Point p2) {
        return (p1.getX() == p2.getX() && p1.getY() == p2.getY());
    }
};

#endif /* POINT_H_ */

/*! \class Point Point.cuh "include/model/Point.cuh"
 *  \brief This is a simple class for representing point.
 */
