/*
 * PointTest.cu
 *
 *  Created on: 21 lut 2018
 *      Author: wpater
 */

#include "gtest/gtest.cuh"
#include "model/Point.cuh"

namespace {

class PointTest: public ::testing::Test {

protected:
    BigNumber n1, n2, n3, n4;
    Point p1, p2;

    void SetUp() {
        n1 = BigNumber("3");
        n2 = BigNumber("0");
        n3 = BigNumber("3");
        n4 = BigNumber("0");
        p1 = Point(n1, n2);
        p2 = Point(n3, n4);
    }

    void TearDown() {}
};

/**
 * Description:     Compare two Points.
 * Expected value:  (bool) true
 */
TEST_F(PointTest, Compare) {
    ASSERT_TRUE(p1 == p2);
}
}
