#!/bin/bash

MAKE=0
CLEAN=0

while :; do
	case "$1" in
		--make|-m)
			MAKE=1
			;;
		--clean|-c)
			CLEAN=1
			;;
		--help|-h)
			echo "Script for running test in Ulam project.
    Usage: [-m|--make] [-c|--clean] [-h|--help]
        -m | --make    Compile files
        -c | --clean   Clean dirs before and after
        -h | --help    Print this help"
			exit 1
			;;
		*)
			break
			;;
	esac
	shift
done

cd ..

if [ $CLEAN == 1 ]; then
	printf "\nCleaning...\n\n"
	make clean > /dev/null
	printf "==================================================================\n"
fi

if [ $MAKE == 1 ] || [ $CLEAN == 1 ]; then
	printf "\nBuilding...\n\n"
	make test
	printf "\n==================================================================\n"
fi

printf "\nTesting...\n\n"
bin/test
printf "\n==================================================================\n"

if [ $CLEAN == 1 ]; then
	printf "\nCleaning...\n\n"
	make clean > /dev/null
	printf "==================================================================\n"
fi
printf "\nDone.\n\n"
