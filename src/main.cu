#include <stdio.h>
#include <iostream>
#include "model/Point.cuh"
#include "model/Spiral.cuh"
#include "model/BigNumberCUDA.cuh"
#include "spdlog/spdlog.h"

#include <cxxopts/cxxopts.cuh>
#include "ulam/utility.cuh"
#include "model/BigNumber.cuh"
#include "ulam/ulamCPU.cuh"
#include "cuda/helper_cuda.h"

//template< int NUM_THREADS_PER_BLOCK, size_t N > __global__ void run_kernel(
//        BigNumberCUDA<N> * number, BigNumberCUDA<N> * number2, int * result) {
//    *result = number->test();
//    number->add(number2);
//    number->addNumber(number2);
//}

//bool runOnCUDA(int warp_size) {
//    BigNumber * cpuNumber1 = new BigNumber("1");
//    BigNumber * cpuNumber2 = new BigNumber("5");
//    short n1[] = { 1 };
//    short n2[] = { 5 };
//    BigNumberCUDA<1> * number1 = new BigNumberCUDA<1>(1, n1);
//    BigNumberCUDA<1> * number2 = new BigNumberCUDA<1>(5, n2);
//
//    cout << endl << "Array 1 before: " << number1->getNumber()[0] << "\n";
//    cout << endl << "Array 2 before: " << number2->getNumber()[0] << "\n";
//
//    BigNumberCUDA<1> * number1_device, *number2_device;
//    short * array1, *array2;
//    int * result;
//    int r;
//    checkCudaErrors(cudaMalloc((void **) &number1_device, sizeof(BigNumberCUDA<1> )));
//    checkCudaErrors(cudaMemcpy(number1_device, number1, sizeof(BigNumberCUDA<1> ), cudaMemcpyHostToDevice));
//
//    checkCudaErrors(cudaMalloc((void **) &array1, sizeof(short)));
//    checkCudaErrors(cudaMemcpy(array1, n1, sizeof(short), cudaMemcpyHostToDevice));
//
//    checkCudaErrors(cudaMalloc((void **) &number2_device, sizeof(BigNumberCUDA<1> )));
//    checkCudaErrors(cudaMemcpy(number2_device, number2, sizeof(BigNumberCUDA<1> ), cudaMemcpyHostToDevice));
//
//    checkCudaErrors(cudaMalloc((void **) &result, sizeof(int)));
//
//    const int NUM_THREADS_PER_BLOCK = 128; // Do not use less than 128 threads.
//    const int NUM_WARPS_PER_BLOCK = NUM_THREADS_PER_BLOCK / warp_size;
//    const size_t smem_size = 4 * NUM_WARPS_PER_BLOCK * sizeof(int);
//
//    // run_kernel<NUM_THREADS_PER_BLOCK, 1><<<1, NUM_THREADS_PER_BLOCK, smem_size>>>(number1_device, number2_device, result);
//    run_kernel<1, 1><<<1, 1, smem_size>>>(number1_device, number2_device, result);
//    checkCudaErrors (cudaGetLastError());
//
//checkCudaErrors    (cudaMemcpy(&r, result, sizeof(int), cudaMemcpyDeviceToHost));
//    checkCudaErrors(cudaMemcpy(number1, number1_device, sizeof(BigNumberCUDA<1> ), cudaMemcpyDeviceToHost));
//    cout << endl << "Result for simple test: " << r << "\n\n";
//    cout << endl << "Result from class for simple adding two ints: " << number1->getValue() << "\n\n";
//    cout << endl << "Vector 1 after: " << number1->getNumber()[0] << "\n";
//    cout << endl << "Vector 2 after: " << number2->getNumber()[0] << "\n\n";
//
//    return true;
//}

int main(int argc, const char *argv[]) {
    //-------------------------------------------------------------------------------------------------------------SETUP
    utility::createLogger();
    shared_ptr<spdlog::logger> logger = spdlog::get("logger");
    std::cout << "\n\t\t\tWelcome in ULAM Spiral simulator!\n\n";

    // Parse program parameters
    cxxopts::Options options(argv[0], "Factorize numbers with algorithm using Ulam spiral");
    options.add_options()("h,help", "Prints this help")("cpu", "Runs algotihm on CPU")("e,example", "Runs few examples")(
            "v,verbose", "More printouts")("c,config", "Configuration file with number and other options",
            cxxopts::value<std::string>());
    cxxopts::ParseResult result = options.parse(argc, argv);
    if (result.count("help")) {
        std::cout << options.help() << std::endl;
        exit(0);
    }
    bool example = false;
    if (result.count("example")) {
        example = true;
    }
    bool cpu = false;
    if (result.count("cpu")) {
        std::cout << "Factorization on CPU" << std::endl;
        cpu = true;
    }
    std::string config = "";
    if (result.count("config")) {
        config = result["c"].as<std::string>();
        std::cout << "Configuration file: " << config << "\n\n";
    } else if (!example) {
        std::cout << "Configuration file not set." << std::endl;
        exit(1);
    }
    BigNumber task = utility::readConfiguration(config);
    std::cout << "Configuration file parsed and task is prepared: " << task.toString() << ".\n\n";
    logger->info("Configuration file {} parsed and task is prepared: {}.", config, task.toString());
    //-----------------------------------------------------------------------------------------------------------EXAMPLE
    if (example) {
        utility::example();
        utility::exampleBigNumber();
    }
    //----------------------------------------------------------------------------------------------------RESOLVE ON CPU
    if (cpu) {
        ulamcpu::resolve(task);
    }
    //----------------------------------------------------------------------------------------------------RESOLVE ON GPU
    else {
        // Find/set the device.
        // The test requires an architecture SM35 or greater (CDP capable).
        int cuda_device = findCudaDevice(argc, (const char **) argv);
        cudaDeviceProp deviceProps;
        checkCudaErrors(cudaGetDeviceProperties(&deviceProps, cuda_device));
        int cdpCapable = (deviceProps.major == 3 && deviceProps.minor >= 5) || deviceProps.major >= 4;

        printf("GPU device %s has compute capabilities (SM %d.%d)\n", deviceProps.name, deviceProps.major,
                deviceProps.minor);

        if (!cdpCapable) {
            std::cerr << "cdpQuadTree requires SM 3.5 or higher to use CUDA Dynamic Parallelism.  Exiting...\n"
                    << std::endl;
            exit(EXIT_WAIVED);
        }

//	bool ok = runOnCUDA(deviceProps.warpSize);

//	return (ok ? EXIT_SUCCESS : EXIT_FAILURE);
    }
}
