/*
 * Spiral.cuh
 *
 *  Created on: 12 sie 2017
 *      Author: wpater
 */

#ifndef SPIRAL_CUH_
#define SPIRAL_CUH_

#include <vector>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include "model/Point.cuh"
#include "model/BigNumber.cuh"
#include "spdlog/spdlog.h"

class Spiral {

    /** @brief Holds size for Ulam Spiral.
     * `size` means count of numbers in Spiral. */
    int size;
    /** @brief Holds values in Spiral. */
    std::vector<int> values;
    /** @brief Holds logger for debugging. */
    shared_ptr<spdlog::logger> debug_logger;
    /** @brief Holds BigNumber instance of 0. */
    BigNumber zero = BigNumber();
    /** @brief Holds BigNumber instance of 1. */
    BigNumber one = BigNumber("1");
    /** @brief Holds BigNumber instance of 2. */
    BigNumber two = BigNumber("2");
    /** @brief Holds BigNumber instance of 3. */
    BigNumber three = BigNumber("3");
    /** @brief Holds BigNumber instance of 4. */
    BigNumber four = BigNumber("4");
    /** @brief Holds BigNumber instance of 5. */
    BigNumber five = BigNumber("5");
    /** @brief Holds BigNumber instance of 6. */
    BigNumber six = BigNumber("6");
    /** @brief Holds BigNumber instance of 7. */
    BigNumber seven = BigNumber("7");
    /** @brief Holds BigNumber instance of 8. */
    BigNumber eight = BigNumber("8");

public:
    /** @brief Default constructor.
     *  @return a Spiral class with size = 0. */
    Spiral();

    /**	@brief Spiral constructor. Parameter `s` is count of numbers in Ulam Spiral.
     *  @warning Not supported yet.
     *  @param s a BigNumber - it's size of Ulam Spiral
     *  @return a Spiral class with specified size. */
    Spiral(BigNumber);

    /** @brief Spiral constructor. Parameter `s` is count of numbers in Ulam Spiral.
     *  @param s an int - it's size of Ulam Spiral
     *  @return a Spiral class with specified size. */
    Spiral(int);

    /**	@brief Default destructor. */
    virtual ~Spiral();

    /** @brief Returns size of Ulam Spiral.
     *  @return an integer. */
    int getSize();

    /** @brief Returns number for corresponding point in Ulam Spiral.
     *  @param p a Point
     *  @return a BigNumber. */
    BigNumber getNumber(Point);

    /** @brief Returns Point for corresponding number in Ulam Spiral.
     *  @param number a BigNumber
     *  @return a Point. */
    Point getPoint(BigNumber);

    /** @brief Prints whole Spiral on standard output.
     *
     * Example Spiral with `size = 43` :
     *
     * 	Ulam spiral
     *
     *			Size: 43
     *
     *			Dim: 7 x 7
     *
     *
     *		|36    35    34    33    32    31    30    |
     *
     *  	|37    16    15    14    13    12    29    |
     *
     *		|38    17    4     3     2     11    28    |
     *
     *		|39    18    5     0     1     10    27    |
     *
     *		|40    19    6     7     8     9     26    |
     *
     *		|41    20    21    22    23    24    25    |
     *
     *		|42                                        |
     *
     *	It is not printed in standard logs due to bad readability.
     *		*/
    void printSpiral();
};

#endif /* SPIRAL_CUH_ */

/*! \class Spiral Spiral.cuh "include/model/Spiral.cuh"
 *  \brief This is a class for Ulam Spiral.
 *
 * Elemental class for representanting Ulam Spiral.
 * 
 * To find Point/number for corresponding number/Point it's enough to
 * create empty Spiral (by default constructor) - and this is recommended option.
 * 
 * Constructor with `size` parameter is needed only for printing whole Spiral.
 * 
 * Spiral class has not any 2D array for values. All numbers can be hold
 * in simple vector. 2D array is needed only for printing Ulam Spiral.
 * 
 * Getting value from array for specific Point could be faster than calculating
 * it but library should be prepared for really big numbers. It could be
 * a problem with performance in case when all numbers are stored in a memory.
 * 
 * 	TODO: more specific description is needed
 */
