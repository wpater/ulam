/*
 * Spiral.cu
 *
 *  Created on: 12 sie 2017
 *      Author: wpater
 */

#include "model/Spiral.cuh"
using namespace std;

Spiral::Spiral() :
        size(0) {
    debug_logger = spdlog::get("debug_logger");
}

Spiral::Spiral(BigNumber s) :
        size(0), values(0) {
    debug_logger = spdlog::get("debug_logger");
    // In this case I don't support printing table, values are not needed.
}

Spiral::Spiral(int s) :
        size(s), values(s) {
    debug_logger = spdlog::get("debug_logger");
    for (int i = 0; i < size; i++) {
        values.push_back(i);
    }
}

Spiral::~Spiral() {
    // TODO Auto-generated destructor stub
}

int Spiral::getSize() {
    return size;
}

/**
 * There is special algorithm for finding number for given Point:
 * * Calculate `m` = max of |x| and |y| coordinates from given Point
 * * Calculate parameter `k = 4 * m * (m - 1)`
 * * For:
 * 	* `m == x` returns `k + m + y`
 * 	* `m == y` returns `k + 3 * m -x`
 * 	* `m == -x` returns `k + 5 * m - y`
 * 	* `m == -y` returns `k + 7 * m + x`
 *
 * Algorithm depends on coordinates where Spiral is started
 * and where it is going.
 */
BigNumber Spiral::getNumber(Point p) {
    BigNumber x = p.getX(), y = p.getY();
    BigNumber minus_x = -x, minus_y = -y;
    BigNumber x_abs = x.abs(), y_abs = y.abs();

    BigNumber m = x_abs >= y_abs ? x_abs : y_abs;

    BigNumber k = m.multiply(four).multiply(m.subtract(one)), number;
    if (m == x)
        number = k.add(m.add(y));
    if (m == y)
        number = k.add(m.multiply(three).subtract(x));
    if (m == minus_x)
        number = k.add(m.multiply(five).subtract(y));
    if (m == minus_y)
        number = k.add(m.multiply(seven).add(x));

    debug_logger->debug("Number on ({}, {}) is {}", x.toString(), y.toString(), number.toString());
    return number;
}

/**
 * There is special algorithm for finding Point for given number:
 * * For `0` returns `Point(0, 0)`
 * * Calculate `m = floor((sqrt(number) + 1) / 2)`
 * * Calculate `k = number - 4 * m * (m -1)`
 * * For:
 * 	* `1 <= k <= 2 * m` returns `Point(m, k - m)`
 * 	* `2 * m <= k <= 4 * m` returns `Point(3 * m - k, m)`
 * 	* `4 * m <= k <= 6 * m` returns `Point(-m, 5 * m - k)`
 * 	* `6 * m <= k <= 8 * m` returns `Point(k - 7 * m, -m)`
 *
 * Algorithm depends on coordinates where Spiral is started
 * and where it is going.
 */
Point Spiral::getPoint(BigNumber number) {
    BigNumber x = BigNumber(), y = BigNumber();
    if (number == zero) {
        return Point(x, y);
    }
    BigNumber m = number.sqrt().add(one).divide(two);
    BigNumber k = number.subtract(four.multiply(m).multiply(m.subtract(one)));
    BigNumber minus_m = -m;

    if (one <= k && k <= (two.multiply(m))) {
        x = m;
        y = k.subtract(m);
    } else if ((two.multiply(m)) <= k && k <= four.multiply(m)) {
        x = three.multiply(m).subtract(k);
        y = m;
    } else if (four.multiply(m) <= k && k <= six.multiply(m)) {
        x = minus_m;
        y = five.multiply(m).subtract(k);
    } else if (six.multiply(m) <= k && k <= eight.multiply(m)) {
        x = k.subtract(seven.multiply(m));
        y = minus_m;
    }

    debug_logger->debug("Point for number {} is ({}, {})", number.toString(), x.toString(), y.toString());
    return Point(x, y);
}

void Spiral::printSpiral() {
    int dir = 1;
    int dim = sqrt(size + 0.) + 1;
    debug_logger->debug("Printing Ulam spiral with: size={} and Dim={}x{}", size, dim, dim);
    printf("Ulam spiral\n\tSize: %d\n\tDim: %d x %d\n\n", size, dim, dim);
    int ** table = new int *[dim];
    for (int i = 0; i < dim; ++i)
        table[i] = new int[dim];
    for (int i = 0; i < dim; ++i) {
        for (int j = 0; j < dim; ++j) {
            table[i][j] = -1;
        }
    }
    int x = dim / 2;
    int y = dim % 2 == 0 ? x - 1 : x;
    for (int i = 0; i < size; i++) {
        table[x][y] = i;
        switch (dir) {
        // Right
        case 1:
            if (table[x][y + 1] == -1) {
                dir = 2;
            }
            ;
            break;
            // Up
        case 2:
            if (table[x - 1][y] == -1) {
                dir = 3;
            }
            ;
            break;
            // Left
        case 3:
            if (table[x][y - 1] == -1) {
                dir = 4;
            }
            ;
            break;
            // Down
        case 4:
            if (table[x + 1][y] == -1) {
                dir = 1;
            }
            ;
            break;
        }
        // Change cord depends on direction
        switch (dir) {
        case 1:
            x += 1;
            break;
        case 2:
            y += 1;
            break;
        case 3:
            x -= 1;
            break;
        case 4:
            y -= 1;
            break;
        }
    }
    // Print whole table
    for (int i = 0; i < dim; i++) {
        cout << left << setw(1) << setfill(' ') << "|";
        for (int j = 0; j < dim; j++) {
            if (table[i][j] == -1) {
                cout << left << setw(6) << setfill(' ') << " ";
            } else {
                cout << left << setw(6) << setfill(' ') << table[i][j];
            }
        }
        cout << left << setw(1) << setfill(' ') << "|\n";
    }
}
