/*
 * utility.cu
 *
 *  Created on: 22 lut 2018
 *      Author: wpater
 */

#include "ulam/utility.cuh"

namespace utility {

BigNumber readConfiguration(string filename) {
    string line, number, base = "10";
    fstream config(filename);
    if (config.is_open()) {
        while (getline(config, line)) {
            if (line.find("number=") != std::string::npos) {
                number = line.substr(line.find("=") + 1);
            }
            if (line.find("base=") != std::string::npos) {
                base = line.substr(line.find("=") + 1);
            }
        }
        config.close();
    } else
        std::cout << "Unable to open file\n";
    return BigNumber(std::stoi(base), number);
}

void createLogger() {
    auto debug_logger = spdlog::rotating_logger_mt("debug_logger", "logs/test_debug", 1024 * 1024 * 25, 3);
    debug_logger->set_level(spdlog::level::debug);
    auto logger = spdlog::rotating_logger_mt("logger", "logs/test_info", 1024 * 1024 * 5, 2);
    logger->info("---------------------------- Welcome in ULAM Spiral simulator! ----------------------------");
}

void example() {
    Spiral s(768);

    BigNumber px("3");
    BigNumber py("0");
    Point p(px, py);

    BigNumber n2 = s.getNumber(p);
    Point p2 = s.getPoint(n2);
    s.printSpiral();
    std::cout << "\n\tNumber " << n2.toString() << " is at " << p2.toString() << "\n\n";
    //BigNumber big("1197397");
    BigNumber big("17");
    std::cout << "\n\tPoint of " << big.toString() << " is " << s.getPoint(big).toString() << "\n\n";
    BigNumber big_p("311");
    std::cout << "\n\tPoint of " << big_p.toString() << " is " << s.getPoint(big_p).toString() << "\n\n";
    BigNumber big_q("509");
    std::cout << "\n\tPoint of " << big_q.toString() << " is " << s.getPoint(big_q).toString() << "\n\n";
}

void exampleBigNumber() {
    BigNumber n1("1:1:37");
    std::cout << "\n\tNumber " << n1.toString() << "\n\n";
    BigNumber n2("2");
    std::cout << "\n\tNumber " << n2.toString() << "\n\n";
}

string bigNumberVectorToString(vector<BigNumber> v) {
    string s = "[";
    if (v.size() > 0) {
        for (BigNumber n : v) {
            s.append(n.toString());
            s.append(", ");
        }
        // Remove last space and ,
        s.pop_back();
        s.pop_back();
    }
    s.append("]");
    return s;
}

}
